/* CLICopti.i */
/*
 *  This file is part of CLICopti.
 *
 *  Authors: Kyrre Sjobak, Daniel Schulte, Alexej Grudiev, Andrea Latina, Jim Ögren
 *
 *  We have invested a lot of time and effort in creating the CLICopti library,
 *  please cite it when using it; see the CITATION file for more information.
 *
 *  CLICopti is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  CLICopti is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public License
 *  along with CLICopti.  If not, see <https://www.gnu.org/licenses/>.
 */

%{
#include <string>
#include <sstream>
%}
%include <std_string.i>

%{
#include "cellParams.h"
#include "cellBase.h"
#include "constants.h"
#include "structure.h"

extern void init_structure_calculator();
%}

#ifdef SWIGPYTHON
//Exception handling like proposed in
// https://stackoverflow.com/a/1513274
%include exception.i
%{
  static PyObject* pAccelStructureUninitialized;
  static PyObject* pAccelStructureInternalError;
  static PyObject* pCellBaseError;
  static PyObject* pCellParamsError;
%}
#endif

// Ignore these operators explicitly; we provide explicit wrapping with __repr__() below.
%ignore operator<<(std::ostream &out, const CellParams& cell);
%ignore operator<<(std::ostream &out, const return_AccelStructure_getMaxFields& cell);
%ignore operator<<(std::ostream &out, const return_AccelStructure_getMaxDeltaT& cell);
%ignore operator<<(std::ostream &out, const return_AccelStructure_getMaxAllowableBeamTime_detailed& cell);

//Explicitly ignore these methods, they don't really make sense to call from Python
%ignore getByOffset(CellParams const *const,size_t const);
%ignore getByOffset(CellParams const &,size_t const);

//Ignore these methods, not strictly needed from Python
//(they are later sort of renamed in SWIG so that they become accessible from Python)
%ignore operator*(const CellParams& lhs, const double rhs);
%ignore operator*(const double lhs, const CellParams& rhs);
%ignore operator/(const CellParams& lhs, const double rhs);
%ignore operator+(const CellParams& lhs, const CellParams& rhs);
%ignore operator-(const CellParams& lhs, const CellParams& rhs);

// Used for passing offsets to DB interpolator
%include "carrays.i"
%array_class(size_t, size_t_array)
%array_class(double, double_array)
// In modernized cases, we use std::vector instead
// Including this enables automatic conversion of tuples to vector, as needed
%include "std_vector.i"
namespace std {
  %template(size_t_vector) vector<size_t>;
  %template(double_vector) vector<double>;
}

#ifdef SWIGPYTHON
//Exception handler for functions that follow this declaration
%exception {
  try {
    $action
  }
  catch(AccelStructureUninitialized &e) {
    PyErr_SetString(pAccelStructureUninitialized, const_cast<char*>(e.what()));
    SWIG_fail;
  }
  catch(AccelStructureInternalError &e) {
    PyErr_SetString(pAccelStructureInternalError, const_cast<char*>(e.what()));
    SWIG_fail;
  }
  catch (CellBaseError &e) {
    PyErr_SetString(pCellBaseError, const_cast<char*>(e.what()));
    SWIG_fail;
  }
  catch (CellParamsError &e) {
    PyErr_SetString(pCellParamsError, const_cast<char*>(e.what()));
    SWIG_fail;
  }
  catch(std::domain_error &e) {
    SWIG_exception(SWIG_ValueError, const_cast<char*>(e.what()));
    SWIG_fail;
  }
  catch(std::invalid_argument &e) {
    SWIG_exception(SWIG_ValueError, const_cast<char*>(e.what()));
    SWIG_fail;
  }
  catch(std::out_of_range &e) {
    SWIG_exception(SWIG_ValueError, const_cast<char*>(e.what()));
    SWIG_fail;
  }
}
#endif

%include "cellParams.h"
%include "cellBase.h"
%include "constants.h"
%include "structure.h"

%ignore init_structure_calculator;

%init %{
  #ifdef SWIGPYTHON
    pAccelStructureUninitialized = PyErr_NewException("_CLICopti.AccelStructureUninitialized", NULL, NULL);
    Py_INCREF(pAccelStructureUninitialized);
    PyModule_AddObject(m, "AccelStructureUninitialized", pAccelStructureUninitialized);

    pAccelStructureInternalError = PyErr_NewException("_CLICopti.AccelStructureInternalError", NULL, NULL);
    Py_INCREF(pAccelStructureInternalError);
    PyModule_AddObject(m, "AccelStructureInternalError", pAccelStructureInternalError);

    pCellBaseError = PyErr_NewException("_CLICopti.CellBaseError", NULL, NULL);
    Py_INCREF(pCellBaseError);
    PyModule_AddObject(m, "_CLICopti.CellBaseError", pCellBaseError);

    pCellParamsError = PyErr_NewException("_CLICopti.CellParamsError", NULL, NULL);
    Py_INCREF(pCellParamsError);
    PyModule_AddObject(m, "_CLICopti.CellParamsError", pCellParamsError);
  #endif

  init_structure_calculator();
%}

#ifdef SWIGPYTHON

// Convert the pretty printing functions to pythonic forms
%extend CellParams {
public:
    std::string __repr__() {
      std::ostringstream out;
      out << *$self;
      return out.str();
  }
}

%extend return_AccelStructure_getMaxFields {
public:
  std::string __repr__() {
    std::ostringstream out;
    out << *$self;
    return out.str();
  }
}

%extend return_AccelStructure_getMaxDeltaT {
  public:
  std::string __repr__() {
    std::ostringstream out;
    out << *$self;
    return out.str();
  }
}

%extend return_AccelStructure_getMaxAllowableBeamTime_detailed {
  public:
  std::string __repr__() {
    std::ostringstream out;
    out << *$self;
    return out.str();
  }
}

%extend return_AccelStructure_getMaxFields {
  public:
  std::string __repr__() {
    std::ostringstream out;
    out << *$self;
    return out.str();
  }
}

%extend return_AccelStructure_getMaxDeltaT {
  public:
  std::string __repr__() {
    std::ostringstream out;
    out << *$self;
    return out.str();
  }
}

%extend return_AccelStructure_getMaxAllowableBeamTime_detailed {
  public:
  std::string __repr__() {
    std::ostringstream out;
    out << *$self;
    return out.str();
  }
}

//Convert the various mathematical operators
// for CellParams to Pythonic forms
%extend CellParams {
  public:
  CellParams __mul__(const double rhs) {
    return (*$self) * rhs;
  }
  CellParams __rmul__(const double lhs) {
    return lhs * (*$self);
  }
  CellParams __truediv__(const double rhs) {
    return (*$self) / rhs;
  }
  CellParams __add__(const CellParams& rhs) {
    return (*$self) + rhs;
  }
  CellParams __sub__(const CellParams& rhs) {
    return (*$self) - rhs;
  }
}
#endif
