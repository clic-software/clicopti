# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'CLICopti RF structure calculator'
copyright = '2014-2024, K. Sjobak et al.'
author = 'K. Sjobak et al.'

#Find the project
import os
import sys
sys.path.insert(0,os.path.abspath('../../../Python/'))
#import beamGeneratorLibrary

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    'sphinx.ext.autodoc',
    'sphinx.ext.coverage',
    'sphinx.ext.napoleon'
]

templates_path = ['_templates']
exclude_patterns = []



# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'agogo'
#html_static_path = ['_static']

#autoclass_content = "both"

autodoc_member_order = 'bysource'