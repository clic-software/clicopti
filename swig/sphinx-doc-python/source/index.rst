Introduction
=============

This describes the CLICopti Python interface, which is a wrapper around the CLICopti library.
The library and the methods implemented therein is described in `CLIC note 1028 <https://cds.cern.ch/record/1712948>`_;
please see this documents for further details.

The documentation is for the most part automatically generated from
Doxygen comments in the C++ source code, which is pulled into Python docstrings using SWIG.
This is then interpreted by Sphinx, which can generate documentation in HTML,
PDF (LaTeX), and several other formats.

Python modules
===============

There are four Python modules in the ``CLICopti`` library,
corresponding to the four header files in the C++ code.
Each of them are documented below.

To use the Python interface, first do ``import CLICopti``,
then the submodules are accessible as e.g. ``CLICopti.CellParams``.
The special submodule ``CLICopti.CLICopti`` contains most of the members of all submodules;
it is how the underlying C++ library is loaded into Python.

RFStructure
-----------

This module contains the logic for evaluating an RF accelerator structure.


The top level base class ``AccelStructure``:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: CLICopti.RFStructure.AccelStructure
    :members:
    :undoc-members:
    :special-members: __repr__
    :exclude-members: thisown

Daugther classes
~~~~~~~~~~~~~~~~

.. autoclass:: CLICopti.RFStructure.AccelStructure_paramSet1
    :members:
    :undoc-members:
    :exclude-members: thisown

.. autoclass:: CLICopti.RFStructure.AccelStructure_paramSet2
    :members:
    :undoc-members:
    :exclude-members: thisown

.. autoclass:: CLICopti.RFStructure.AccelStructure_paramSet2_noPsi
    :members:
    :undoc-members:
    :exclude-members: thisown

.. autoclass:: CLICopti.RFStructure.AccelStructure_CLIC502
    :members:
    :undoc-members:
    :exclude-members: thisown

.. autoclass:: CLICopti.RFStructure.AccelStructure_CLICG
    :members:
    :undoc-members:
    :exclude-members: thisown

.. autoclass:: CLICopti.RFStructure.AccelStructure_general
    :members:
    :undoc-members:
    :exclude-members: thisown

Return data structures
~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: CLICopti.RFStructure.return_AccelStructure_getMaxFields
    :members:
    :undoc-members:
    :special-members: __repr__
    :exclude-members: thisown

.. autoclass:: CLICopti.RFStructure.return_AccelStructure_getMaxDeltaT
    :members:
    :undoc-members:
    :special-members: __repr__
    :exclude-members: thisown

.. autoclass:: CLICopti.RFStructure.return_AccelStructure_getMaxAllowableBeamTime_detailed
    :members:
    :undoc-members:
    :special-members: __repr__
    :exclude-members: thisown

.. Add in case something else shows up
.. automodule:: CLICopti.RFStructure
    :members:
    :undoc-members:

CellParams
----------

This module contains a single struct, containing the parameters of a single cell
(and equivalently, a point along the :math:`z` axis of the structure).
This struct is meant to be created by a ``CellBase`` or ``AccelStructure``,
not directly in Python.

It is possible to get a pretty-printed string representation of such structs by
running `str(myCell)`, or indirectly by `print(myCell)`.

The data type ``CellParams`` is fundamental to the inner workings of rest of the library.
This is due to Cell objects being used to store the loaded points in the ``CellBase`` classes,
and to do the arithmetic needed for creating and returning interpolated points from ``CellBase::getCellInterpolated(...)``.
Further, objects of the ``CellParams`` type are used multiple places in the ``AccelStructure`` class,
primarily to hold the interpolation points needed to describe the structure parameters
as continuous functions along the length of the structure.

.. autoclass:: CLICopti.CellParams.CellParams
    :members:
    :undoc-members:
    :special-members: __repr__
    :exclude-members: thisown

For some functions such as ``CellBase`` instances and
``AccelStructure::getInterpolated()``, ``AccelStructure::getInterpolated_zidx()``,
and ``AccelStructure::getInterpolatedZero()``, offsets into ``CellParams`` are used to
specify which fields are accessed.
The offsets are stored in constants named ``off_XXX`` where ``XXX`` is the field name from
the ``CellParams`` structure.
Aditionally, there is a class for transmitting arrays of offsets:

.. autoclass:: CLICopti.CellParams.size_t_array
    :members:
    :exclude-members: thisown

.. Add in case something else shows up
.. automodule:: CLICopti.CellParams
    :members:
    :undoc-members:

CellBase
--------

This module contains the `CellBase` classes, used for creating `CellParam` structures
by interpolation of tables of pre-calculated cells.
This is typically used by some of the constructors of `AccelStructure`.
A ``CellBase`` object loads a database file on start, and uses that to extract 

The top level class is ``CellBase``:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autoclass:: CLICopti.CellBase.CellBase
    :members:
    :undoc-members:
    :exclude-members: thisown


A number of loaders and interpolators are available:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
These are initialized by the user, which loads the table of structures (slow).
Once a structure object has been built, it can be used 


.. autoclass:: CLICopti.CellBase.CellBase_grid
    :members:
    :undoc-members:
    :exclude-members: thisown

.. autoclass:: CLICopti.CellBase.CellBase_compat
    :members:
    :undoc-members:
    :exclude-members: thisown

.. autoclass:: CLICopti.CellBase.CellBase_linearInterpolation
    :members:
    :undoc-members:
    :exclude-members: thisown

.. autoclass:: CLICopti.CellBase.CellBase_linearInterpolation_freqScaling
    :members:
    :undoc-members:
    :exclude-members: thisown

.. Add in case something else shows up
.. automodule:: CLICopti.CellBase
    :members:
    :undoc-members:

Cell Database files
~~~~~~~~~~~~~~~~~~~
Pre-calculated data for the cells are stored in database files,
as discussed in `CLIC note 1026 <https://cds.cern.ch/record/1712945>`_.
The CellBase constructors expect the paths to these as arguments;
these are stored in the following variables:

* ``CLICopti.CellBase.celldatabase_TD_12GHz_v1``
* ``CLICopti.CellBase.celldatabase_TD_12GHz_v2``
* ``CLICopti.CellBase.celldatabase_TD_30GHz``

Constants
---------

.. Add in case something else shows up
.. automodule:: CLICopti.Constants
    :members:
    :undoc-members:

Tips and Tricks
===============

As a treat for reading the manual:
You can disable the splash screen about citation that appears when importing CLICopti by setting the environment variable `CLICOPTI_NOSPLASH`.
Thus, the following results in a quiet import:

>>> import os
>>> os.environ['CLICOPTI_NOSPLASH'] = 'YES'
>>> import CLICopti

We trust that you will handle citation correctly.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

.. toctree::
   :maxdepth: 2
   :caption: Contents:
