import numpy as np
import matplotlib.pyplot as plt
import scipy.interpolate as ipol

#CLIC_G data
h = 8.332 #mm
N = 26
L = N*h
vg_points = [1.65, 1.2, 0.83]
z_points = [0, L/2.0, L]

z = np.linspace(0,L)
vg = ipol.lagrange(z_points, vg_points)
zCell = z/h#+0.5

#Load interpolation from MATLAB
vgFile = open("vg_alexejInterpol.dat", 'r')
vg_mat = []
for line in vgFile:
    vg_mat.append(float(line))
vgFile.close()
assert (len(vg_mat) == N)


#plt.axvline(0, ls='--', color='k')
#plt.axvline(L/2, ls='--', color='k')
#plt.axvline(L, ls='--', color='k')

for i in xrange(N+1):
#    plt.axvline(i*8.332, ls='--', color='k')
    if i == 1:
        plt.axvline(h*i, ls='--', color='k', label="Cell iris")
    else:
        plt.axvline(h*i, ls='--', color='k')

plt.plot(z,vg(z), 'r', label="Lagrange interpolant")
plt.step(h*(np.asarray([0.5]+range(1,N+1)+[N+0.5])-0.5), [vg_mat[0]] + vg_mat + [vg_mat[-1]], 'b', where='mid', label="MATLAB")

plt.plot(z_points,vg_points, 'r*', ms=15)
plt.plot(h*np.asarray((0.5, np.floor(N/2.0)-0.5, N-0.5)), vg_points, 'b*', ms=15)

plt.xlim(0-L*0.025,L*1.025)
#plt.xlim(0-N*0.1,N*1.1)
#plt.ylim(0,max(vg_points)*1.1)
plt.ylim(min(vg_points)*0.9, max(vg_points)*1.1)

#plt.xlabel("Cell idx (0..26)")
plt.xlabel("z [mm]")
plt.ylabel(r" $v_g$ [%c]")

plt.legend()

plt.savefig("InterpolCompareVg.pdf")

plt.show()
