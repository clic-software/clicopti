import matplotlib.pyplot as plt
import numpy as np

#N = 22
N = 24
#h = 10.41467e-3
h = 8.3316e-3

L=N*h

#Gradient through a CLIC_G structure as calculated by my old Python code.
# Target is average loaded gradient = 80 MV/m
# at 2.1789599856 Amps, requirering 74.19 MW of input power.
# This yields a (square) efficiency of 53.83 %.
pyGrad = open('gradient_clicG.dat','r')
py_z = []
py_cellZ = []
py_guload = []
py_gload = []
for line in pyGrad.readlines():
    ls = line.split()
    py_z.append(float(ls[0]))
    py_cellZ.append(float(ls[1]))
    py_guload.append(float(ls[2]))
    py_gload.append(float(ls[3]))
pyGrad.close()

#Gradient through the same structure as calculated by Alexej's MATLAB code.
# Same targets as above, but I=2.174948266666667 as electron charge 1.6e-19 C
matGrad = open('matlab_clicG_gradient.dat', 'r')
mat_cellZ = []
mat_gload = []
mat_guload = []
l = 1
for line in matGrad.readlines():
    ls = line.split()
    if len(ls) == 0: 
        continue
    mat_cellZ.append(float(l))
    #mat_gload.append(float(ls[0]))
    mat_gload.append(float(ls[1])) #G: Switched the columns in the file.
    #mat_guload.append(float(ls[1]))
    mat_guload.append(float(ls[0]))
    l+=1
matGrad.close()

#Same structure, c++ code.
cppGrad = open("testfile_acsG_unloaded.dat", 'r')
cpp_z = []
cpp_cellZ = []
cpp_guload = []
for line in cppGrad.readlines():
    if line.startswith("#"):
        continue
    ls = line.split()
    if len(ls) == 0:
        continue
    cpp_z.append(float(ls[0]))
    cpp_cellZ.append(cpp_z[-1]/h+0.5)
    cpp_guload.append(float(ls[1]))
cppGrad.close()

cppGrad = open("testfile_acsG_loaded.dat", 'r')
cpp2_z = []
cpp2_cellZ = []
cpp2_gload = []
for line in cppGrad.readlines():
    if line.startswith("#"):
        continue
    ls = line.split()
    if len(ls) == 0:
        continue
    cpp2_z.append(float(ls[0]))
    cpp2_cellZ.append(cpp2_z[-1]/h+0.5)
    cpp2_gload.append(float(ls[1]))
cppGrad.close()

plt.plot(py_z, py_guload, 'r-', label="Python, unloaded")
plt.step(1e3*h*(np.asarray([0.5] + mat_cellZ + [N+0.5])-0.5), [mat_guload[0]] + mat_guload + [mat_guload[-1]], c='r', ls="--", where='mid', label="MATLAB, unloaded")
plt.plot(np.asarray(cpp_z)*1e3, cpp_guload, 'r-.', label="CLICopti, unloaded")
plt.plot(py_z, py_gload, 'b-', label="Python, loaded")
plt.step(1e3*h*(np.asarray([0.5] + mat_cellZ + [N+0.5])-0.5), [mat_gload[0]] + mat_gload + [mat_gload[-1]], c='b',ls="--", where='mid', label="MATLAB, loaded")
plt.plot(np.asarray(cpp2_z)*1e3, cpp2_gload, 'b-.', label="CLICopti, loaded")
for i in xrange(0,N+1):
    if i == 0:
        plt.axvline(i*h*1e3, ls='--', c='k', label="Cell iris")        
    else:
        plt.axvline(i*h*1e3, ls='--', c='k')
plt.xlabel("z [mm]")
plt.ylabel("Gradient [MV/m]")
plt.xlim(-L*0.025*1e3,L*1.025*1e3)
plt.legend(loc=3)

plt.savefig("gradientCompare.pdf")

plt.show()
