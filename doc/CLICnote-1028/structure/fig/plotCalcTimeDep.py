#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt

#######################################
## POWER TIME PROFILE AT DIFFERENT z ##
#######################################

fname = "testfile_acsG_R05_parameterProfile.dat"
(z, a, Q, vg, rQ, Es, Hs, Sc, f1mn, Q1mn, A1mn) = np.loadtxt(fname, unpack=True)
z *= 1000
L = z[-1]

#input power profile
# fname_power = "testfile_acsG_R05_timePowerProfile.dat"

# ifile = open(fname_power,'r')
# headerline =  ifile.readline()
# ifile.close()
# print "Got header line: '" + headerline + "'"
# headerline = headerline.split()
# P0 = float(headerline[3].split('=')[1][:-5])
# Pbreak = float(headerline[4].split('=')[1][:-5])
# tbeam = float(headerline[5].split('=')[1][:-4])
# print "P0     =", P0,     "[MW]"
# print "Pbreak =", Pbreak, "[MW]"
# print "tbeam  =", tbeam,  "[ns]"

omega = 11.9942e9*2*np.pi #s^-1
c=299792458 #m/s

setGL = 100  # MV/m, setpoint for gradient
I     = 1.19 # A   , setpoint for beam current

#(t,P) = np.loadtxt(fname_power, unpack=True)
#plt.plot(t*1e9,P,zorder=1000)



#I      = 1.19    #A
#P0     = 60.3516 #MW

#g(z)
import scipy as sp
import scipy.integrate

#Integral in the exponent of g(z)
# The integral
Ia  = np.empty_like(z); Ia[0] = 0.0
# Integrand 
Iap = omega / (vg*c*0.01 * Q)
for i in xrange(1,len(z)):
    #Simpson's rule from the beginning (SLOW - but it's OK)
    Ia[i] = sp.integrate.simps(Iap[:i+1], x=z[:i+1]/1000)
#g(z) (units in prefactor anyway cancels)
gz = np.sqrt( (vg[0] * rQ) / (vg * rQ[0]) ) * np.exp(-0.5*Ia);
gz_int = sp.integrate.simps(gz, x=z/1000)
del Iap

#g_L(z)
gL = np.empty_like(z);gL[0] = 0.0
# Integrand
Ibp = (omega * rQ) / (gz * 2*vg*c*0.01)
for i in xrange(1,len(z)):
    #Simpson's rule from the beginning (SLOW - but it's OK)
    gL[i] = sp.integrate.simps(Ibp[:i+1], x=z[:i+1]/1000)
gL *= gz
gL_int = sp.integrate.simps(gL, x=z/1000)
#del Ibp #Also used for time-dep stuff

#tests (Steady state):
P0 = ( (vg[0]*c*0.01) / (omega * rQ[0]) ) * ( (setGL*1e6*L*1e-3 + I*gL_int) / gz_int )**2
P0 *= 1e-6
print "P0 =", P0, "MW"
G0 = np.sqrt( (omega * rQ[0] * P0*1e6) / (vg[0]*c*0.01) )

plt.figure()
plt.plot(z,G0*gz/1e6) #MV/m
plt.plot(z,(G0*gz - I*gL)/1e6) #MV/m
plt.xlim(0,L)
plt.xlabel("z [mm]")
plt.ylabel("G [MV/m]")

print "average gradient =", sp.integrate.simps(G0*gz/1e6, x=z/1000) / (L/1000), " MV/m (unloaded)"
print "average gradient =", sp.integrate.simps((G0*gz - I*gL)/1e6, x=z/1000) / (L/1000), " MV/m (loaded)"

### Transient ### 


#propagation time from z'=0 to z
tz = np.empty_like(z); tz[0] = 0.0;
Itz = 1/(vg*c*0.01)
for i in xrange(1,len(z)):
    #Simpson's rule from the beginning (SLOW - but it's OK)
    tz[i] = sp.integrate.simps(Itz[:i+1], x=z[:i+1]/1000)
del Itz
plt.figure()
plt.plot(z,tz*1e9)
plt.xlim(0,L)
plt.xlabel("z [mm]")
plt.ylabel("t(z) [ns]")
#print "tz[-1] =", tz[-1]*1e9, "[ns], tf =", tf, "[ns]"


Pbreak = P0*(1.0-np.sqrt((vg[0]*c*0.01)/(omega * rQ[0] * P0*1e6)) * I * gL[-1]/gz[-1])**2 #23.2538 #MW
print "Pbreak =", Pbreak, "MW"

tr = 21.0
tf = tz[-1]*1e9 #61.9257 #ns
print "tf =", tf, "ns"
tb = 0.5*312
t1 = tr
t2 = tr+tf
t3 = tr+tf+tb
t4 = tr+tf+tb+tr
t5 = tr+tf+tb+tr+tf

#Linear approximation transient
def getPt(t): #MW(ns)
    if t < 0.0:
        return 0.0
    elif t < t1:
        return Pbreak*t/tr
    elif t < t2:
        return (P0-Pbreak)/tf*(t-t1) + Pbreak
    elif t < t3:
        return P0
    elif t < t4:
        return -Pbreak/tr*(t-t3) + P0
    elif t < t5:
        return (Pbreak-P0)/tf*(t-t4) + P0-Pbreak
    #t >= t5
    return 0.0
def getG0t(t): #V/m (?)
    return np.sqrt( (omega * rQ[0] * getPt(t)*1e6) / (vg[0]*c*0.01) )
## TIME AXIS USED FOR MOST CALCULATIONS!
T = np.linspace(0,t5+tf,401) #ns
P  = map(getPt,T)

# #Actual transient (testing)
# T2_1 = np.linspace(0,t1,11)
# P2_1 = np.asarray(map(lambda t: Pbreak*t/tr, T2_1)) #linear rise
# T2_2 = np.empty_like(z)
# P2_2 = np.empty_like(z)
# for iz in xrange(len(z)):
#     T2_2[iz] = tr + tf - tz[iz]*1e9
#     P2_2[iz] = P0*(1.0 - I/G0 * gL[iz]/gz[iz])**2
#     #print iz, T2_2[iz], P2_2[iz], gL[iz], gz[iz], gL[iz]/gz[iz]
# T2_2 = T2_2[::-1]
# P2_2 = P2_2[::-1]
# T2_3 = np.linspace(t2,t3,10)
# P2_3 = np.ones_like(T2_3)*P0
# T2_4 = T2_1 + t3
# P2_4 = P0-P2_1
# T2_5 = T2_2 - t1 + t4
# P2_5 = P0-P2_2
# T2 = np.append(T2_1, np.append(T2_2, np.append(T2_3, np.append(T2_4, T2_5)))); del T2_1, T2_2, T2_3, T2_4, T2_5
# P2 = np.append(P2_1, np.append(P2_2, np.append(P2_3, np.append(P2_4, P2_5)))); del P2_1, P2_2, P2_3, P2_4, P2_5

def getPt2_BLcomp(tpp): #MW(ns), tpp = \int_z^L dz'/vg(z') = tf-\int_0^z dz'/vg(z')
    tpp *=1e-9 #s
    #print tpp, tz[0], tz[-1]
    if not (tpp < tz[-1]):
        print "getPt2_BLcomp(): Warning: tpp < tz[-1], returning P0!"
        return P0
    assert tpp > tz[0]
    
    #find z(tpp)
    tppp = tz[-1]-tpp #reverse
    zpp = None
    gLoverg = None
    for it in xrange(1,len(tz)):
        if (tz[it] > tppp):
            zpp  = z[it-1]  + (tppp-tz[it-1])*(z[it]-z[it-1])/(tz[it]-tz[it-1])
            gLpp = gL[it-1] + (tppp-tz[it-1])*(gL[it]-gL[it-1])/(tz[it]-tz[it-1])
            gpp  = gz[it-1] + (tppp-tz[it-1])*(gz[it]-gz[it-1])/(tz[it]-tz[it-1])
            gLoverg = gLpp/gpp
            break
    assert zpp     != None
    assert gLoverg != None
    return P0*(1 - I/G0 * gLoverg)**2
def getPt2(t): #MW(ns), compensated pulse
    if t < 0.0:
        return 0.0
    elif t < t1:
        return Pbreak*t/tr
    elif t < t2:
        return getPt2_BLcomp(t-t1)
    elif t < t3:
        return P0
    elif t < t4:
        return -Pbreak/tr*(t-t3) + P0
    elif t < t5:
        return P0-getPt2_BLcomp(t-t4)
    #t >= t5
    return 0.0
def getG02t(t): #V/m (?)
    return np.sqrt( (omega * rQ[0] * getPt2(t)*1e6) / (vg[0]*c*0.01) )


def getIt(t): #A(ns)
    if t<t2 or t>t3:
        return 0.0
    return I

#T3 = np.linspace(0,t5+tf,501) #ns
P3  = map(getPt2,T)

## Power as function of time

plt.figure()
plt.plot(T,P, label="Piecewise linear")
#plt.plot(T2,P2,'r--', label="Ideal")  #for testing
plt.plot(T,P3,'r--', label="Ideal")

plt.axvline(t1, ls='--',color="black")
plt.annotate("$t_1$", (t1,5))

plt.axvline(t2, ls='--',color="black")
plt.annotate("$t_2$", (t2,5))

plt.axvline(t3, ls='--',color="black")
plt.annotate("$t_3$", (t3,5))

plt.axvline(t4, ls='--',color="black")
plt.annotate("$t_4$", (t4,5))

plt.axvline(t5, ls='--',color="black")
plt.annotate("$t_5$", (t5,5))

powerTextXpos = 150
plt.axhline(P0, ls='--', color='black')
plt.annotate("$P_0$",(powerTextXpos,P0+1), ha='center')

plt.axhline(Pbreak, ls='--', color='black')
plt.annotate(r"$P_\mathrm{start}$",(powerTextXpos,Pbreak+1), ha='center')

plt.axhline(P0-Pbreak, ls='--', color='black')
plt.annotate(r"$P_0 - P_\mathrm{start}$",(powerTextXpos,P0-Pbreak+1), ha='center')

plt.annotate('', xy=(0,30), xycoords = 'data', xytext = (t1, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_r$', xy=(tr/2.0,32), ha='center')

plt.annotate('', xy=(t1,30), xycoords = 'data', xytext = (t2, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_f$', xy=(t1+tf/2.0,32), ha='center')

plt.annotate('', xy=(t2,30), xycoords = 'data', xytext = (t3, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_b$', xy=(t2+tb/2.0,32), ha='center')

plt.annotate('', xy=(t3,30), xycoords = 'data', xytext = (t4, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_r$', xy=(t3+tr/2.0,32), ha='center')

plt.annotate('', xy=(t4,30), xycoords = 'data', xytext = (t5, 30), textcoords = 'data', arrowprops = {'arrowstyle':'<->'})
plt.annotate('$t_f$', xy=(t4+tf/2.0,32), ha='center')

plt.axhline(P0*0.85, ls='--', color='orange')
plt.annotate("0.85 $P_0$",(powerTextXpos,52), ha='center',color='orange')

t85_1 = t1 + (0.85*P0-Pbreak)*(t2-t1) / (P0-Pbreak) 
t85_2 = t3 + (0.85*P0-P0)*(t3-t4) / Pbreak
plt.axvline(t85_1,ls='--',color='orange')
plt.axvline(t85_2,ls='--',color='orange')

pulselengthTextY = 0.85*P0
plt.annotate('', xy=(t85_1,pulselengthTextY), xycoords = 'data', xytext = (t85_2, pulselengthTextY), textcoords = 'data', arrowprops = {'arrowstyle':'<->', 'color':'orange', 'linewidth':2})
plt.annotate("Counted for pulse length $\\tau$",(powerTextXpos,pulselengthTextY-1),ha='center',va='top',color='orange')


plt.xlabel("t [ns]")
plt.ylabel("P [MW]")
plt.legend(loc='center', bbox_to_anchor = (t2+(t3-t2)/2, 10), bbox_transform=plt.gca().transData)
plt.xlim(0,350)

plt.savefig("powerProfile_v2.pdf")


##Drive beam current as function of time
Idb_0 = 1.0
kdb   = P0/Idb_0**2
print "kdb =", kdb, "MW/A^2"
def getIdb(t):
    if t < 0.0:
        return 0.0
    elif t < t1:
        return np.sqrt((Pbreak/kdb) * (t/tr))
    elif t < t2:
        return np.sqrt( ((P0-Pbreak)/tf * (t-t1) + Pbreak)/kdb)
    elif t < t3:
        return Idb_0
    elif t < t5:
        return Idb_0 - getIdb(t-t3)
    # elif t < t5:
    #     return Idb_0 - getIdb(t-t4+t1)
    return 0.0
Idb = np.asarray(map(getIdb,T))

Idb_power = kdb*Idb**2

plt.figure()
plt.plot(T,np.sqrt(map(getPt,T)/kdb),label="Piecewise linear $P(t)$")
plt.plot(T,Idb,'r--', label="Drive beam")

plt.axvline(t1, ls='--',color="black")
plt.annotate("$t_1$", (t1,0.2))

plt.axvline(t2, ls='--',color="black")
plt.annotate("$t_2$", (t2,0.2))

plt.axvline(t3, ls='--',color="black")
plt.annotate("$t_3$", (t3,0.2))

plt.axvline(t4, ls='--',color="black")
plt.annotate("$t_4$", (t4,0.2))

plt.axvline(t5, ls='--',color="black")
plt.annotate("$t_5$", (t5,0.2))

plt.annotate(r"$I_\mathrm{DB}(P_0)$",(powerTextXpos,Idb_0+0.025), ha='center')
plt.axhline(Idb_0,ls='--',color="black")

plt.annotate(r"$I_\mathrm{DB}(P_\mathrm{start})$",(powerTextXpos, np.sqrt(Pbreak/kdb)+0.025), ha='center')
plt.axhline(np.sqrt(Pbreak/kdb),ls='--',color="black")

plt.annotate(r"$I_\mathrm{DB}(P_0) - I_\mathrm{DB}(P_\mathrm{start})$",(powerTextXpos,Idb_0-np.sqrt(Pbreak/kdb)+0.025), ha='center')
plt.axhline(Idb_0-np.sqrt(Pbreak/kdb),ls='--',color="black")

plt.xlim(0,350)
plt.ylim(0,1.1)
plt.xlabel("t [ns]")
plt.ylabel(r"$I_\mathrm{DB}/I_\mathrm{DB}(P_0)$")
plt.legend(loc=0)

plt.savefig("drivebeamCurrent.pdf")

plt.figure()
plt.plot(T,P, label="Piecewice linear")
plt.plot(T,Idb_power, 'r--' , label="Drive beam")

plt.axvline(t1, ls='--',color="black")
plt.annotate("$t_1$", (t1,5))

plt.axvline(t2, ls='--',color="black")
plt.annotate("$t_2$", (t2,5))

plt.axvline(t3, ls='--',color="black")
plt.annotate("$t_3$", (t3,5))

plt.axvline(t4, ls='--',color="black")
plt.annotate("$t_4$", (t4,5))

plt.axvline(t5, ls='--',color="black")
plt.annotate("$t_5$", (t5,5))

plt.annotate(r"$I_\mathrm{DB}(P_0)$",(powerTextXpos,P0+1), ha='center')
plt.axhline(P0,ls='--',color="black")

plt.annotate(r"$P_\mathrm{start}$",(powerTextXpos, Pbreak+1), ha='center')
plt.axhline(Pbreak,ls='--',color="black")

plt.annotate(r"$P(I_\mathrm{DB}(P_0) - I_\mathrm{DB}(P_\mathrm{start}))$",(powerTextXpos,kdb*(Idb_0-np.sqrt(Pbreak/kdb))**2+1), ha='center')
plt.axhline(kdb*(Idb_0-np.sqrt(Pbreak/kdb))**2,ls='--',color="black")


plt.xlim(0,350)
plt.xlabel("t [ns]")
plt.ylabel(r"P [MW]")
plt.legend(loc='center', bbox_to_anchor = (0.45, 0.5))

plt.savefig("drivebeamPower.pdf")


#plt.show()

### SOLVERS : COMMENT OUT ONE ! ###

print "Solving time-dependent problem"
Gtz  = np.empty((len(T),len(z))) #unloaded - first index (row) is time, second (col) is z
GtzL = np.zeros((len(T),len(z))) #loaded   - first index (row) is time, second (col) is z
for it in xrange(len(T)):
    if it % 10 == 0:
        print float(it)/(len(T)-1) * 100, "%"
    for iz in xrange(len(z)):
        teff = T[it]*1e-9 - tz[iz] #s
        if teff < 0:
            Gtz[it,iz] = 0.0
        else:
            Gtz[it,iz] = getG0t(teff*1e9)*gz[iz] #Linear power pulse
            Gtz[it,iz] = getG02t(teff*1e9)*gz[iz] #Fully compensated
        
        #Loading
        if iz > 0:
            It = map(getIt,(teff+tz[:iz+1])*1e9)
            GtzL[it,iz] = gz[iz]*sp.integrate.simps(It*Ibp[:iz+1], x=z[:iz+1]/1000)#*gz[iz]

iz0 = 0        #z=0
iz1 = len(z)/2 #z=L/2
iz2 = len(z)-1 #z=L

plt.figure()
plt.plot(T,Gtz[:,iz0]/1e6,label="z=0")
plt.plot(T,Gtz[:,iz1]/1e6,label="z=L/2")
plt.plot(T,Gtz[:,iz2]/1e6,label="z=L")
plt.xlabel("t [ns]")
plt.ylabel("Unloaded G [MV/m]")
plt.legend(loc=0)

plt.figure()
plt.plot(T-tz[iz0]*1e9,Gtz[:,iz0]/1e6,label="z=0")
plt.plot(T-tz[iz1]*1e9,Gtz[:,iz1]/1e6,label="z=L/2")
plt.plot(T-tz[iz2]*1e9,Gtz[:,iz2]/1e6,label="z=L")
plt.xlabel("t-t(z) [ns]")
plt.ylabel("Unloaded G [MV/m]")
plt.legend(loc=0)

plt.figure()
plt.plot(T,GtzL[:,iz0]/1e6,label="z=0")
plt.plot(T,GtzL[:,iz1]/1e6,label="z=L/2")
plt.plot(T,GtzL[:,iz2]/1e6,label="z=L")
plt.xlabel("t [ns]")
plt.ylabel("Loading [MV/m]")
#(plot_ymin,plot_ymax) = plt.ylim()
plt.ylim(-10,50)
plt.legend(loc=0)

plt.figure()
plt.plot(T-tz[iz0]*1e9,GtzL[:,iz0]/1e6,label="z=0")
plt.plot(T-tz[iz1]*1e9,GtzL[:,iz1]/1e6,label="z=L/2")
plt.plot(T-tz[iz2]*1e9,GtzL[:,iz2]/1e6,label="z=L")
plt.xlabel("t - t(z) [ns]")
plt.ylabel("Loading [MV/m]")
plt.ylim(-10,50)
plt.legend(loc=0)

plt.figure()
plt.plot(T,(Gtz[:,iz0]-GtzL[:,iz0])/1e6,label="z=0")
plt.plot(T,(Gtz[:,iz1]-GtzL[:,iz1])/1e6,label="z=L/2")
plt.plot(T,(Gtz[:,iz2]-GtzL[:,iz2])/1e6,label="z=L")

plt.xlabel("t [ns]")
plt.ylabel("Loaded G [MV/m]")
plt.legend(loc=0)

plt.figure()
plt.plot(T-tz[iz0]*1e9,(Gtz[:,iz0]-GtzL[:,iz0])/1e6,label="z=0")
plt.plot(T-tz[iz1]*1e9,(Gtz[:,iz1]-GtzL[:,iz1])/1e6,label="z=L/2")
plt.plot(T-tz[iz2]*1e9,(Gtz[:,iz2]-GtzL[:,iz2])/1e6,label="z=L")
plt.xlabel("t - t(z) [ns]")
plt.ylabel("Loaded G [MV/m]")
plt.legend(loc=0)

plt.figure()
plt.plot(T,Gtz[:,iz0]/1e6,'b-',label="z=0")
plt.plot(T,Gtz[:,iz1]/1e6,'g-',label="z=L/2")
plt.plot(T,Gtz[:,iz2]/1e6,'r-',label="z=L")
plt.plot(T,(Gtz[:,iz0]-GtzL[:,iz0])/1e6,'b--')
plt.plot(T,(Gtz[:,iz1]-GtzL[:,iz1])/1e6,'g--')
plt.plot(T,(Gtz[:,iz2]-GtzL[:,iz2])/1e6,'r--')

plt.axvline(t2, ls='--',color="black")
plt.annotate("$t_2$", (t2,20))
plt.axvline(t3, ls='--',color="black")
plt.annotate("$t_3$", (t3,20))

plt.xlabel("t [ns]")
plt.ylabel("G [MV/m]")
plt.legend(loc=0)

plt.savefig("GradientProfiles_timedep.pdf")

(Tmesh,Zmesh) = np.meshgrid(z,T)
plt.figure()
plt.contourf(Tmesh,Zmesh,Gtz/1e6,50)
plt.colorbar().set_label("Unloaded G [MV/m]")
plt.xlabel("z [mm]")
plt.ylabel("t [ns]")
plt.axhline(t1,ls='--',color='black')
plt.axhline(t2,ls='--',color='black')
plt.axhline(t3,ls='--',color='black')
plt.axhline(t4,ls='--',color='black')

plt.figure()
#(Tmesh,Zmesh) = np.meshgrid(z,T)
plt.contourf(Tmesh,Zmesh,GtzL/1e6,50)
plt.colorbar().set_label("Loading [MV/m]")
plt.xlabel("z [mm]")
plt.ylabel("t [ns]")
plt.axhline(t1,ls='--',color='black')
plt.axhline(t2,ls='--',color='black')
plt.axhline(t3,ls='--',color='black')
plt.axhline(t4,ls='--',color='black')

plt.figure()
#(Tmesh,Zmesh) = np.meshgrid(z,T)
plt.contourf(Tmesh,Zmesh,(Gtz-GtzL)/1e6,50)
plt.colorbar().set_label("Loaded G [MV/m]")
plt.xlabel("z [mm]")
plt.ylabel("t [ns]")
plt.axhline(t1,ls='--',color='black')
plt.annotate("$t_1$", (75,t1+5))
plt.axhline(t2,ls='--',color='black')
plt.annotate("$t_2$", (75,t2+5))
plt.axhline(t3,ls='--',color='black')
plt.annotate("$t_3$", (75,t3+5))
plt.axhline(t4,ls='--',color='black')
plt.annotate("$t_4$", (75,t4+5))
plt.axhline(t5,ls='--',color='black')
plt.annotate("$t_5$", (75,t5+5))

plt.savefig("GradientProfiles_timedep_contours.pdf")

#Voltage as a function of time
Vul = np.zeros_like(T)
VL  = np.zeros_like(T)
for it in xrange(len(T)):
    Vul[it] = sp.integrate.simps(Gtz [it,:], x=z/1000)
    VL [it] = sp.integrate.simps(GtzL[it,:], x=z/1000)
    
plt.figure()
plt.plot(T,(Vul/1e6)/(L/1000)     , label='Unloaded')
plt.plot(T,(VL/1e6)/(L/1000)      , label='Loading')
plt.plot(T,((Vul-VL)/1e6)/(L/1000), label='Loaded')
plt.xlabel("t [ns]")
plt.ylabel("V [MV/m]")
plt.legend(loc=0)

plt.savefig("VoltageTime.pdf")

plt.show()
