# Publishing to CLICopti pyPi

The CLICopti project is published to pyPI.
Currently this is done manually on a local machine; in the future it would be desireable to do this as a CI/CD action triggered by GitLab, on infrastructure.

## Step 0: Preparation
The new version of the package must have a larger version number in the `pyproject.toml` file, as described in https://packaging.python.org/en/latest/specifications/version-specifiers/#version-specifiers
Remember to also update the version number in `swig/splash.cc` and remake the SWIG files.

Furthermore, the new version must work. Do a local pip install and test it.

To make sure we have a clean build directory, let's checkout the code to a new folder, i.e.
```
cd
mkdir code-build
cd code-build
git clone ssh://git@gitlab.cern.ch:7999/clic-software/clicopti.git
cd clicopti
```

## Step 1: Building
Build the software to a distributable package using the following command in the root of the repository:
```
python -m build
```

This creates some files in the `build` subfolder, i.e. `clicopti-2.1.1.tar.gz` and `CLICopti-2.1.1-cp38-cp38-linux_x86_64.whl`.

The first file is a multiplatform source distribution, the second is a precompiled version for a specific linux and python version.

Documentation:
https://packaging.python.org/en/latest/tutorials/packaging-projects/

## Step 2A: Building wheels on Linux

On Linux, a standardized ABI is used when building wheels, so that it will be compatible to a maximal amount of distributions.
To achieve this, they are built inside a container.

As an example, let's get the manylinux2014 container for x86_64:
```
podman pull quay.io/pypa/manylinux2014_x86_64
```
We can see that the download was successful, and we now have the image ID `f12ed5025e16`:
```
$ podman images
REPOSITORY                         TAG         IMAGE ID      CREATED       SIZE
quay.io/pypa/manylinux2014_x86_64  latest      f12ed5025e16  29 hours ago  1.23 GB
```
Note that I'm using podman not docker since I am on Fedora, but the results are the same.

Let's then now boot this container with the source repo mounted:
```
podman run -it -v $(pwd):/io:Z f12ed5025e16
```
I'm using the `:Z` flag on the mount in order to get around some SELinux restrictions.

From this, I can set my Python version, e.g.
```
PYBIN=/opt/python/cp38-cp38/bin/
```
And then I can build:
```
cd /io/clicopti
${PYBIN}/python -m build
```
This produces the `.whl` and `.tar.gz` files listed above, in the `build` folder.
The next step is then to make the generated `.whl` into a generic `manylinux` type wheel.
This is done by the `auditwheel` command:
```
# auditwheel show dist/CLICopti-2.1.1-cp38-cp38-linux_x86_64.whl

CLICopti-2.1.1-cp38-cp38-linux_x86_64.whl is consistent with the
following platform tag: "manylinux_2_17_x86_64".

The wheel references external versioned symbols in these
system-provided shared libraries: libgcc_s.so.1 with versions
{'GCC_3.0'}, libpthread.so.0 with versions {'GLIBC_2.2.5'}, libc.so.6
with versions {'GLIBC_2.4', 'GLIBC_2.14', 'GLIBC_2.2.5'}, libm.so.6
with versions {'GLIBC_2.2.5'}, libstdc++.so.6 with versions
{'GLIBCXX_3.4.9', 'GLIBCXX_3.4.11', 'CXXABI_1.3', 'GLIBCXX_3.4'}

This constrains the platform tag to "manylinux_2_17_x86_64". In order
to achieve a more compatible tag, you would need to recompile a new
wheel from source on a system with earlier versions of these
libraries, such as a recent manylinux image.
```

OK, so this wheel is OK. Let's fix the namings etc.:
```
# auditwheel repair dist/CLICopti-2.1.1-cp38-cp38-linux_x86_64.whl 
INFO:auditwheel.main_repair:Repairing CLICopti-2.1.1-cp38-cp38-linux_x86_64.whl
INFO:auditwheel.wheeltools:Previous filename tags: linux_x86_64
INFO:auditwheel.wheeltools:New filename tags: manylinux_2_17_x86_64, manylinux2014_x86_64
INFO:auditwheel.wheeltools:Previous WHEEL info tags: cp38-cp38-linux_x86_64
INFO:auditwheel.wheeltools:New WHEEL info tags: cp38-cp38-manylinux_2_17_x86_64, cp38-cp38-manylinux2014_x86_64
INFO:auditwheel.main_repair:
Fixed-up wheel written to /io/clicopti/wheelhouse/CLICopti-2.1.1-cp38-cp38-manylinux_2_17_x86_64.manylinux2014_x86_64.whl
```

Done. Now we have a new file `CLICopti-2.1.1-cp38-cp38-manylinux_2_17_x86_64.manylinux2014_x86_64.whl` in the `wheelhouse` folder.

Documentation I've followed for this:
http://www.martin-rdz.de/index.php/2022/02/05/manually-building-manylinux-python-wheels/

List of relevant images to use (TODO: Add to this):
* quay.io/pypa/manylinux2014_x86_64

## Step 2B: Building wheels on other operating systems

TODO

## Step 3: Publishing to pyPi

I will do the publishing using TWINE. First, I have configured it for PyPi, using an API key. First I generated the key on my PyPi profile, then I created a file `.pypirc` in my home folder containing this, i.e.
```
[pypi]
  username = __token__
  password = pypi-(ETC....)
```

I installed TWINE to system Python (user install) on my Fedora 41 machine:
``` pip install twine```

Then I can publish both the `.tar.gz` and the manylinux `.whl` files:
```
python -m twine upload dist/clicopti-2.1.1.tar.gz
```
```
python -m twine upload wheelhouse/CLICopti-2.1.1-cp38-cp38-manylinux_2_17_x86_64.manylinux2014_x86_64.whl
```

If I try to upload one of the machine/distro specific `.whl` files, I get the following error:
```
$ python -m twine upload dist/*
Uploading distributions to https://upload.pypi.org/legacy/
Uploading CLICopti-2.1.1-cp38-cp38-linux_x86_64.whl
100% ━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━ 1.2/1.2 MB • 00:01 • 1.4 MB/s
WARNING  Error during upload. Retry with the --verbose option for more details.                  
ERROR    HTTPError: 400 Bad Request from https://upload.pypi.org/legacy/                         
         Binary wheel 'CLICopti-2.1.1-cp38-cp38-linux_x86_64.whl' has an unsupported platform tag
         'linux_x86_64'.
```

## Step 4: Sucess!

Having done our first push of CLICopti, it appeared here:
https://pypi.org/project/CLICopti/
We can now install it (assuming either the .tar.gz or a compatible wheel is available) using simply `pip install CLICopti`.

