% load library
CLICopti;

% constants
clight = 299792458; % m/s
Z0 = 377; % ohm, impedance of free space

% lattice parameters
beta = 16; % m, average beta function
E_in = 2.86; % GeV, initial energy
E_fin = 9; % GeV, final energy

% beam parameters
sigma_z = 300e-6; % m, bunch length
n_bunches = 352; % number of bunches per train
n_particles = 5.2e9; % 0.83 nC, numer of electrons per bunch
train_length = 177e-9; % s, total train length

% pulse compressor parameters
pl = [ 200:50:1000 8000 Inf ]; % ns
cr = [ 5.61, 5.26, 4.94, 4.66, 4.40, 4.16, 3.94, 3.74, 3.55, 3.38, ...
       3.21, 3.05, 2.91, 2.77, 2.64, 2.51, 2.39, 1.0, 0.0 ]; % unitless

% klystron paramemeters
power_per_klystron = 80; % MW, for 2 GHz

% RF structure parameters
frequency = 2; % GHz
lambda_rf = clight / frequency / 1e9; % m
a_range = linspace(0.08,  0.22,  15); % a / lambda
d_range = linspace(0.11,  0.40,  9); % d / cell length
G_range = linspace(10, 60, 26) * 1e6; % V/m

FID = fopen("Results.dat","w"); 
if FID != -1

    % auxiliary variables for computing the average wakefield kick
    h = linspace(-3, 3, 128); % longitudinal sampling in units of sigma
    Z = (h - min(h)) * sigma_z; % m, distance from bunch head
    Q = n_particles * (normpdf(h) * diff(h)(1)) / 6.241509125883258e+18; % C, charge distribution

    % init CLICopti and start
    base = CellBase_compat ("../cellBase/TD_12GHz_v2.dat", frequency, false, 3);
    
    function C = cost(rf_units, rf_length)
        Cost_per_length = 50e3; % CHF/m
        Cost_per_klystron = 300e3; % CHF/#
        C = rf_units * (2 * Cost_per_klystron) + rf_length * Cost_per_length; % CHF
    end
    
    % main loop
    for a1 = a_range
        for a2 = a_range
            for d1 = d_range
                for d2 = d_range
                    for G = G_range
                        for n_cell = 10:200

                            as = AccelStructure_paramSet2_noPsi (base, n_cell, 0.5*(a1+a2), a1-a2, 0.5*(d1+d2), d1-d2);
                            as.calc_g_integrals (200);
                            
                            % compute the average wakefield kick
                            a = 0.5 * (a1+a2) * lambda_rf; % m, average aperture
                            Wt = @(z) 2*Z0*clight*z ./ (pi*a*a*(a*a.+z*lambda_rf)); % V/C/m/m, transverse wakefield
                            avg_Kt = mean(conv(Q, Wt(Z))); % V/m/m, average transverse wakefield kick
                            
                            % stability integral
                            A = 0.5 * beta * log(E_fin / E_in) / G * avg_Kt; % unitless
                            
                            if A<0.4 % motion is stable
                                
                                peak_beam_current = n_bunches * n_particles / train_length / 6.241509343260179e+18; % A
                                
                                power_unloaded = as.getPowerUnloaded (G*as.getL()) / 1e6; % MW
                                power_loaded   = as.getPowerLoaded   (G*as.getL(), peak_beam_current) / 1e6; % MW
                                
                                fill_time = as.getTfill () + as.getTrise (); % s
                                t_beam = as.getMaxAllowableBeamTime_detailed (power_loaded*1e6*1.44, peak_beam_current).time; % s
                                
                                if t_beam > train_length % enough time for the bunch train
                                    
                                    pulse_compressor_gain = interp1(pl, cr, fill_time + train_length);
                                    
                                    %pulse_compressor_gain = cr(find(pl > ((fill_time + train_length) * 1e9))(1));
                                    
                                    number_of_AS = ceil((E_fin - E_in) * 1e9 / (G*as.getL()));
                                    number_of_RF_units = number_of_AS*power_loaded/(2*power_per_klystron*pulse_compressor_gain);
                                    
                                    if number_of_RF_units - floor(number_of_RF_units) > 0.9
                                        
                                        efficiency = 100 * as.getTotalEfficiency (power_loaded*1e6, peak_beam_current, train_length); % percent
                                        installed_klystron_power = 2*number_of_RF_units* power_per_klystron; % MW
                                        rf_length = number_of_AS * as.getL(); % m
                                        
                                        fprintf(FID,"%1.5f ", frequency) % GHz
                                        fprintf(FID,"%1.5f ", G/1e6) % MV/m
                                        fprintf(FID,"%1.5f ", as.getL()) % m, struture length
                                        fprintf(FID,"%1.5f ", rf_length) % m, total RF length
                                        fprintf(FID,"%1.5f ", A)
                                        fprintf(FID,"%1.5f ", fill_time*1e9) % ns
                                        fprintf(FID,"%i ",    number_of_AS)
                                        fprintf(FID,"%1.5f ", power_loaded) % MW
                                        fprintf(FID,"%1.5f ", efficiency) % percent
                                        fprintf(FID,"%1.5f ", installed_klystron_power)
                                        fprintf(FID,"%1.5f ", number_of_RF_units)
                                        fprintf(FID,"%1.5f ", cost(number_of_RF_units, rf_length) / 1e6) % MCHF
                                        fprintf(FID,"%1.2f %1.2f %1.2f %1.2f ", a1, a2, d1, d2);
                                        fprintf(FID,"%1.2f\n", 0.5 * (a1+a2));
                                        
                                    end
                                end
                            end
                        end
                    end
                end
            end
        end
    end
    
    fclose(FID);
end