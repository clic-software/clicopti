#include <iostream>
#include <cstddef>
#include <fstream>

#include "cellParams.h"
#include "cellBase.h"

using namespace std;

int main(int argc, char* argv[]) {
  vector<size_t> offsets_30 {
    offsetof(struct CellParams, psi),
    offsetof(struct CellParams, a_n),
    offsetof(struct CellParams, d_n) };

  // !!! USE THIS ONE !!!
  vector<size_t> offsets_12 = {
    offsetof(struct CellParams, a_n),
    offsetof(struct CellParams, d_n)
  };

  cout << "Reading 30 GHz DB..." << endl;
  CellBase_linearInterpolation_freqScaling baseLIN_30scaled  ("cellBase/TD_30GHz.dat", 3, offsets_30, 11.9942);
  CellBase_compat                          basequad_30scaled ("cellBase/TD_30GHz.dat", 11.9942,true);
  cout << "Done." << endl;

  cout << "Reading 12 GHz DB..." << endl;
  CellBase_linearInterpolation_freqScaling baseLIN_12v1scaled("cellBase/TD_12GHz_v1.dat", 2, offsets_12, 11.9942);
  CellBase_linearInterpolation             baseLIN_12v1      ("cellBase/TD_12GHz_v1.dat", 2, offsets_12);
  CellBase_compat                          basequad_12v1     ("cellBase/TD_12GHz_v1.dat", 11.9942, false);
  CellBase_linearInterpolation_freqScaling baseLIN_12v2scaled("cellBase/TD_12GHz_v2.dat", 2, offsets_12, 11.9942);
  CellBase_linearInterpolation             baseLIN_12v2      ("cellBase/TD_12GHz_v2.dat", 2, offsets_12);
  CellBase_compat                          basequad_12v2     ("cellBase/TD_12GHz_v2.dat", 11.9942, false);

  cout << "Mapping out cell data..." << endl;
  ofstream ofile_LIN30scaled  ("testOutput/testCellLibrary_compareDBs_LIN30scaledCellDataDump.dat");
  ofstream ofile_quad30scaled ("testOutput/testCellLibrary_compareDBs_quad30scaledCellDataDump.dat");
  ofstream ofile_LIN12v1scaled("testOutput/testCellLibrary_compareDBs_LIN12v1scaledCellDataDump.dat");
  ofstream ofile_LIN12v1      ("testOutput/testCellLibrary_compareDBs_LIN12v1CellDataDump.dat");
  ofstream ofile_quad12v1     ("testOutput/testCellLibrary_compareDBs_quad12v1CellDataDump.dat");
  ofstream ofile_LIN12v2scaled("testOutput/testCellLibrary_compareDBs_LIN12v2scaledCellDataDump.dat");
  ofstream ofile_LIN12v2      ("testOutput/testCellLibrary_compareDBs_LIN12v2CellDataDump.dat");
  ofstream ofile_quad12v2     ("testOutput/testCellLibrary_compareDBs_quad12v2CellDataDump.dat");
  vector<double> dIdex_30(3);
  dIdex_30[0] = 120;
  vector<double> dIdex_12(2);
  size_t n_an = 61; //HOW MANY A/LAMBDA
  double h_an = (baseLIN_12v1.getMaxLabel(0)-baseLIN_12v1.getMinLabel(0))/(n_an-1);
  size_t n_dn = 31; //HOW MANY D/H
  double h_dn = (baseLIN_12v1.getMaxLabel(1)-baseLIN_12v1.getMinLabel(1))/(n_dn-1);
  for (size_t i_an = 0; i_an < n_an; i_an++) {
    double a_n = baseLIN_12v1.getMinLabel(0)+i_an*h_an;
    a_n > baseLIN_12v1.getMaxLabel(0) ? a_n = baseLIN_12v1.getMaxLabel(0) : 0 /*pass*/ ;
    dIdex_12[0] = dIdex_30[1] = a_n;

    for (size_t i_dn = 0; i_dn < n_dn; i_dn++) {
      double d_n = baseLIN_12v1.getMinLabel(1)+i_dn*h_dn;
      d_n > baseLIN_12v1.getMaxLabel(1) ? d_n = baseLIN_12v1.getMaxLabel(1) : 0/*pass*/ ;
      dIdex_12[1] = dIdex_30[2] = d_n;

      // cout << endl << endl << endl << endl << endl << endl << endl;
      // cout << a_n << " " << d_n << endl;

      CellParams LIN30scaled    = baseLIN_30scaled  .getCellInterpolated(dIdex_30);
      CellParams quad30scaled   = basequad_30scaled .getCellInterpolated(dIdex_30);
      CellParams LIN12v1scaled  = baseLIN_12v1scaled.getCellInterpolated(dIdex_12);
      CellParams LIN12v1        = baseLIN_12v1      .getCellInterpolated(dIdex_12);
      CellParams quad12v1       = basequad_12v1     .getCellInterpolated(dIdex_12);
      CellParams LIN12v2scaled  = baseLIN_12v2scaled.getCellInterpolated(dIdex_12);
      CellParams LIN12v2        = baseLIN_12v2      .getCellInterpolated(dIdex_12);
      CellParams quad12v2       = basequad_12v2     .getCellInterpolated(dIdex_12);

      ofile_LIN30scaled   << a_n              << " " << d_n              << " " << LIN30scaled.a    << " "
                          << LIN30scaled.Q    << " " << LIN30scaled.vg   << " " << LIN30scaled.rQ   << " "
                          << LIN30scaled.Es   << " " << LIN30scaled.Hs   << " " << LIN30scaled.Sc   << " "
                          << LIN30scaled.f1mn << " " << LIN30scaled.Q1mn << " " << LIN30scaled.A1mn << endl;

      ofile_quad30scaled  << a_n               << " " << d_n               << " " << quad30scaled.a    << " "
                          << quad30scaled.Q    << " " << quad30scaled.vg   << " " << quad30scaled.rQ   << " "
                          << quad30scaled.Es   << " " << quad30scaled.Hs   << " " << quad30scaled.Sc   << " "
                          << quad30scaled.f1mn << " " << quad30scaled.Q1mn << " " << quad30scaled.A1mn << endl;

      ofile_LIN12v1scaled << a_n                << " " << d_n                << " " << LIN12v1scaled.a    << " "
                          << LIN12v1scaled.Q    << " " << LIN12v1scaled.vg   << " " << LIN12v1scaled.rQ   << " "
                          << LIN12v1scaled.Es   << " " << LIN12v1scaled.Hs   << " " << LIN12v1scaled.Sc   << " "
                          << LIN12v1scaled.f1mn << " " << LIN12v1scaled.Q1mn << " " << LIN12v1scaled.A1mn << endl;

      ofile_LIN12v1       << a_n                << " " << d_n                << " " << LIN12v1.a          << " "
                          << LIN12v1.Q          << " " << LIN12v1.vg         << " " << LIN12v1.rQ         << " "
                          << LIN12v1.Es         << " " << LIN12v1.Hs         << " " << LIN12v1.Sc         << " "
                          << LIN12v1.f1mn       << " " << LIN12v1.Q1mn       << " " << LIN12v1.A1mn       << endl;

      ofile_quad12v1       << a_n                 << " " << d_n                 << " " << quad12v1.a          << " "
                           << quad12v1.Q          << " " << quad12v1.vg         << " " << quad12v1.rQ         << " "
                           << quad12v1.Es         << " " << quad12v1.Hs         << " " << quad12v1.Sc         << " "
                           << quad12v1.f1mn       << " " << quad12v1.Q1mn       << " " << quad12v1.A1mn       << endl;

      ofile_LIN12v2scaled << a_n                << " " << d_n                << " " << LIN12v2scaled.a    << " "
                          << LIN12v2scaled.Q    << " " << LIN12v2scaled.vg   << " " << LIN12v2scaled.rQ   << " "
                          << LIN12v2scaled.Es   << " " << LIN12v2scaled.Hs   << " " << LIN12v2scaled.Sc   << " "
                          << LIN12v2scaled.f1mn << " " << LIN12v2scaled.Q1mn << " " << LIN12v2scaled.A1mn << endl;

      ofile_LIN12v2       << a_n                << " " << d_n                << " " << LIN12v2.a          << " "
                          << LIN12v2.Q          << " " << LIN12v2.vg         << " " << LIN12v2.rQ         << " "
                          << LIN12v2.Es         << " " << LIN12v2.Hs         << " " << LIN12v2.Sc         << " "
                          << LIN12v2.f1mn       << " " << LIN12v2.Q1mn       << " " << LIN12v2.A1mn       << endl;

      ofile_quad12v2       << a_n                 << " " << d_n                 << " " << quad12v2.a          << " "
                           << quad12v2.Q          << " " << quad12v2.vg         << " " << quad12v2.rQ         << " "
                           << quad12v2.Es         << " " << quad12v2.Hs         << " " << quad12v2.Sc         << " "
                           << quad12v2.f1mn       << " " << quad12v2.Q1mn       << " " << quad12v2.A1mn       << endl;

    }
  }
  ofile_LIN30scaled.close();
  ofile_quad30scaled.close();
  ofile_LIN12v1scaled.close();
  ofile_LIN12v1.close();
  ofile_quad12v1.close();
  ofile_LIN12v2scaled.close();
  ofile_LIN12v2.close();
  ofile_quad12v2.close();

  cout << "Writing database dumps..." << endl;
  ofstream ofile_DB30_scaled   ("testOutput/testCellLibrary_compareDBs_DBdump30_scaled.dat");
  ofstream ofile_DB12v1        ("testOutput/testCellLibrary_compareDBs_DBdump12v1.dat");
  ofstream ofile_DB12v1_scaled ("testOutput/testCellLibrary_compareDBs_DBdump12v1_scaled.dat");
  ofstream ofile_DB12v2        ("testOutput/testCellLibrary_compareDBs_DBdump12v2.dat");
  ofstream ofile_DB12v2_scaled ("testOutput/testCellLibrary_compareDBs_DBdump12v2_scaled.dat");

  std::vector<double> an_list = baseLIN_12v1.getGridlabels(0);
  std::vector<double> dn_list = baseLIN_12v1.getGridlabels(1);
  for (size_t i_an = 0; i_an < baseLIN_12v1.getGridsort(0); i_an++) {
    double a_n = an_list[i_an];
    dIdex_12[0] = dIdex_30[1] = a_n;
    for (size_t i_dn = 0; i_dn < baseLIN_12v1.getGridsort(1); i_dn++) {
      double d_n = dn_list[i_dn];
      dIdex_12[1] = dIdex_30[2] = d_n;

      cout << a_n << " " << d_n << endl;

      CellParams LIN30scaled    = baseLIN_30scaled  .getCellInterpolated(dIdex_30);
      CellParams LIN12v1        = baseLIN_12v1      .getCellInterpolated(dIdex_12);
      CellParams LIN12v1scaled  = baseLIN_12v1scaled.getCellInterpolated(dIdex_12);
      CellParams LIN12v2        = baseLIN_12v2      .getCellInterpolated(dIdex_12);
      CellParams LIN12v2scaled  = baseLIN_12v2scaled.getCellInterpolated(dIdex_12);

      ofile_DB30_scaled   << a_n                << " " << d_n              << " " << LIN30scaled.a    << " "
                          << LIN30scaled.Q      << " " << LIN30scaled.vg   << " " << LIN30scaled.rQ   << " "
                          << LIN30scaled.Es     << " " << LIN30scaled.Hs   << " " << LIN30scaled.Sc   << " "
                          << LIN30scaled.f1mn   << " " << LIN30scaled.Q1mn << " " << LIN30scaled.A1mn << endl;

      ofile_DB12v1        << a_n                << " " << d_n                << " " << LIN12v1.a          << " "
                          << LIN12v1.Q          << " " << LIN12v1.vg         << " " << LIN12v1.rQ         << " "
                          << LIN12v1.Es         << " " << LIN12v1.Hs         << " " << LIN12v1.Sc         << " "
                          << LIN12v1.f1mn       << " " << LIN12v1.Q1mn       << " " << LIN12v1.A1mn       << endl;

      ofile_DB12v1_scaled << a_n                << " " << d_n                << " " << LIN12v1scaled.a    << " "
                          << LIN12v1scaled.Q    << " " << LIN12v1scaled.vg   << " " << LIN12v1scaled.rQ   << " "
                          << LIN12v1scaled.Es   << " " << LIN12v1scaled.Hs   << " " << LIN12v1scaled.Sc   << " "
                          << LIN12v1scaled.f1mn << " " << LIN12v1scaled.Q1mn << " " << LIN12v1scaled.A1mn << endl;

      ofile_DB12v2        << a_n                << " " << d_n                << " " << LIN12v2.a          << " "
                          << LIN12v2.Q          << " " << LIN12v2.vg         << " " << LIN12v2.rQ         << " "
                          << LIN12v2.Es         << " " << LIN12v2.Hs         << " " << LIN12v2.Sc         << " "
                          << LIN12v2.f1mn       << " " << LIN12v2.Q1mn       << " " << LIN12v2.A1mn       << endl;

      ofile_DB12v2_scaled << a_n                << " " << d_n                << " " << LIN12v2scaled.a    << " "
                          << LIN12v2scaled.Q    << " " << LIN12v2scaled.vg   << " " << LIN12v2scaled.rQ   << " "
                          << LIN12v2scaled.Es   << " " << LIN12v2scaled.Hs   << " " << LIN12v2scaled.Sc   << " "
                          << LIN12v2scaled.f1mn << " " << LIN12v2scaled.Q1mn << " " << LIN12v2scaled.A1mn << endl;

    }
  }

  ofile_DB30_scaled  .close();
  ofile_DB12v1       .close();
  ofile_DB12v1_scaled.close();
  ofile_DB12v2       .close();
  ofile_DB12v2_scaled.close();

}
