#include <iostream>
#include <sstream>
#include <fstream>
#include <cstddef>
#include <cassert>
#include <algorithm>

#include "cellParams.h"
#include "cellBase.h"
#include "structure.h"
#include "constants.h"

using namespace std;

int main(int arc, char* argv[]) {
  vector<size_t> offsets = {
    offsetof(struct CellParams, psi),
    offsetof(struct CellParams, a_n),
    offsetof(struct CellParams, d_n) };
  CellBase_linearInterpolation base("cellBase/TD_30GHz.dat", 3, offsets);

  cout << endl << endl << endl;
  cout << "CREATING A STRUCTURE" << endl;
  AccelStructure_paramSet1 acs(&base, 25, 120,0.12,0.015, 0.2);

  cout << "FIRST= " << (acs.getCellFirst()) << endl;
  cout << "MID  = " << (acs.getCellMid())   << endl;
  cout << "LAST = " << (acs.getCellLast())  << endl;
  cout << endl;

  cout << "Length = " << acs.getL() << " [m]" << endl;

  cout << endl << endl << endl;
  cout << "TESTING THE INTERPOLATION" << endl;
  size_t nPoints = 11;
  cout << "v_g(z), " << nPoints << " points" << endl;
  for (size_t i=0; i < nPoints; i++) {
    double z = i*(acs.getL()/(nPoints-1.0));
    cout << i << " " << z << " " << acs.getInterpolated(z,offsetof(struct CellParams, vg)) << endl;
  }

  cout << endl << endl << endl;
  cout << "TESTING THE INTEGRATION" << endl;
  acs.calc_g_integrals(1000);
  cout << "V_ul(1 MW) = " << acs.getVoltageUnloaded(1e6)/1e6 << " [MV]" << endl;
  cout << "t_fill = " << acs.getTfill()*1e9 << " [ns]" << endl;
  cout << "t_rise = " << acs.getTrise()*1e9 << " [ns]" << endl;

  acs.writeProfileFile("/tmp/testfile_acs.dat", 10e6);

  cout << endl << endl << endl;
  cout << "Creating an AccelStructure502" << endl;
  AccelStructure_CLIC502 acs502(22);
  cout << "FIRST=" << (acs502.getCellFirst()) << endl;
  cout << "MID  =" << (acs502.getCellMid()) << endl;
  cout << "LAST =" << (acs502.getCellLast()) << endl;
  cout << endl;

  cout << "Length = " << acs502.getL() << " [m], L/2.0 = " << acs502.getL()/2.0 << " [m]" << endl;

  cout << endl << "Calculating integrals..." << endl;
  acs502.calc_g_integrals(1000);
  cout << endl;

  //Check against old CLIC_502 results, should return 22.9424193839 MV -> 100.131529432 MV/m
  cout << "V_ul(74.1942303887 MW)     = " << acs502.getVoltageUnloaded(74.1942303887e6)/1e6 << " [MV]," << endl
       << " average unloaded gradient = " << acs502.getVoltageUnloaded(74.1942303887e6)/1e6/acs502.getL() << " [MV/m]" << endl;
  cout << endl;

  cout << "Consistency1: 1V -> 1V?: " << acs502.getPowerUnloaded(acs502.getVoltageUnloaded(1.0)) << endl;
  cout << "Consistency2: 1V -> 1V?: " << acs502.getVoltageUnloaded(acs502.getPowerUnloaded(1.0)) << endl;
  cout << endl;

  double I_beam = 1.60217646e-19*6.8e9/0.5e-9; //[A]
  double P0 = 74.1942303887e6; //[W]
  cout << "I_beam = " << I_beam << " [A]" << endl;
  cout << "Beam loading voltage =" << acs502.getLoadingVoltage(I_beam)/1e6 << " [MV]"<< endl;
  cout << "V_l (" << P0/1e6 << " MW)   = " << acs502.getVoltageLoaded(P0, I_beam)/1e6 << " [MV]," << endl
       << " average loaded gradient = " << acs502.getVoltageLoaded(P0, I_beam)/1e6/acs502.getL() << " [MV]" << endl;
  cout << endl;

  //Should get 74.1942303887 MW
  cout << "Power to achieve 80 MV/m loaded gradient = " << acs502.getPowerLoaded(80e6*acs502.getL(), I_beam)/1e6 << " [MW]" << endl;
  cout << " (expected " << P0/1e6 << " MW )" << endl;
  cout << endl;

  //Efficiency (should get 39.6% total at t_rise=15.3ns - OK)
  cout << "RF->beam efficiency (flat-top) = " << acs502.getFlattopEfficiency(P0, I_beam)*100 << "%" << endl;
  cout << "RF->beam efficiency (total)    = " << acs502.getTotalEfficiency(P0, I_beam, 354*0.5e-9)*100 << "%" << endl;
  cout << endl;

  //Must use higher value than beam loading voltage, else power negative!
  cout << "Consistency1: 10MV -> 10MV?: " << acs502.getPowerLoaded(acs502.getVoltageLoaded(10.0e6,I_beam),I_beam) << endl;
  cout << "Consistency2: 10MV -> 10MV?: " << acs502.getVoltageLoaded(acs502.getPowerLoaded(10.0e6,I_beam),I_beam) << endl;
  cout << endl;

  cout << "t_fill = " << acs502.getTfill()*1e9 << " [ns]" << endl;
  cout << "t_rise = " << acs502.getTrise()*1e9 << " [ns]" << endl;
  cout << endl;

  acs502.writeProfileFile("/tmp/testfile_acs502.dat", P0);
  return_AccelStructure_getMaxFields maxFields = acs502.getMaxFields(P0);
  cout << "Unloaded condition:" << endl << maxFields << endl;
  acs502.writeProfileFile("/tmp/testfile_acs502_loaded.dat", P0, I_beam);
  maxFields = acs502.getMaxFields(P0, I_beam);
  cout << "Loaded condition:" << endl << maxFields << endl;
  cout << endl;


  cout << "Loaded condition:" << endl;
  acs502.writeDeltaTprofileFile("/tmp/testfile_acs502_deltaT_loaded.dat", P0, 354*0.5e-9, I_beam, true);
  return_AccelStructure_getMaxDeltaT maxDeltaT = acs502.getMaxDeltaT(P0, 354*0.5e-9, I_beam, true);
  cout << "max deltaT = " << maxDeltaT.maxDeltaT << " K" << endl;
  cout << "Unloaded condition:" << endl;
  acs502.writeDeltaTprofileFile("/tmp/testfile_acs502_deltaT_unloaded.dat", P0, 354*0.5e-9, I_beam, false);
  maxDeltaT = acs502.getMaxDeltaT(P0, 354*0.5e-9, I_beam, false);
  cout << "max deltaT = " << maxDeltaT.maxDeltaT << " K" << endl;
  cout << endl;

  cout << "Writing timePowerProfileFile to '/tmp/testfile_acs502_tProfile.dat'..." << endl;
  acs502.writeTimePowerProfileFile("/tmp/testfile_acs502_tProfile.dat",P0, 354*0.5e-9, I_beam, 5000);
  cout << "Extra time with power > 0.8*P0=" << 0.8*P0/1e6 << " MW: " << acs502.getExtraTimePowerAboveFraction(P0, I_beam, 0.8)*1e9
       << " ns, total time = " << acs502.getExtraTimePowerAboveFraction(P0, I_beam, 0.8)*1e9 + 354*0.5 << " ns" << endl;
  cout << endl;


  //Example of an "optimization":
  cout << "Making plot of deltaT(t_beam), I_beam=" << I_beam << "[A], P0 = " << P0/1e6 << "[MW]..." << endl;
  ofstream deltaTfile("/tmp/testfile_acs502_plotDeltaT.dat");
  deltaTfile << "# bunches t_beam dT_loaded dT_unloaded" << endl;
  acs502.calc_g_integrals(1000);
  for(int bunches = 1; bunches < 1500; bunches += 10) {
    //cout << "\t bunches = " << bunches << endl;
    double t_beam = bunches*0.5e-9;
    double dT_loaded   = (acs502.getMaxDeltaT(P0, t_beam, I_beam, true)).maxDeltaT;
    double dT_unloaded = (acs502.getMaxDeltaT(P0, t_beam, I_beam, false)).maxDeltaT;
    deltaTfile << bunches   << " "
               << t_beam    << " "
               << dT_loaded << " "
               << dT_unloaded << endl;
  }
  deltaTfile.close();
  cout << "Done! Saved to /tmp/testfile_acs502_plotDeltaT.dat" << endl;
  cout << endl;

  //Get maximum power allowable, simplistic limits
  double max_Es = 300.0; //MV/m, at P0 max is 217.54
  double max_Sc = 5.0;   //W/um^2, at P0 max is 4.6
  cout << "Finding maximum power at max_Es=" << max_Es <<" MV/m,"
       <<" max_Sc=" << max_Sc << " W/um^2, I_beam=" << I_beam << " A" << endl;
  acs502.calc_g_integrals(1000);
  double max_P0 = acs502.getMaxAllowablePower(I_beam, max_Es, max_Sc);
  cout << "Got max_P0 = " << max_P0/1e6 << " MW" << endl;
  maxFields = acs502.getMaxFields(P0, I_beam); //TODO: Should be max_P0?
  cout << " Es=" << maxFields.maxEs << " MV/m, Sc_max=" << maxFields.maxSc << " W/um^2" << endl;
  cout << endl << endl;

  //Get max power allowable, full check (useLoadedField=false for dT test)
  cout << "Max beam time for P0 = " << max_P0/1e6 << "MW and I_beam =" << I_beam << " A:" << endl;
  double t_beam_max_auto = acs502.getMaxAllowableBeamTime(max_P0,I_beam); //[s]
  cout << " Got t_beam_max_auto = " << t_beam_max_auto*1e9 << "ns" << endl;
  cout << "Finding maximum power at Ibeam=" <<I_beam<<", beamTime=" << t_beam_max_auto*1e9 << " [ns]" << endl;
  double maxP0_auto = acs502.getMaxAllowablePower_beamTimeFixed(I_beam,t_beam_max_auto);
  cout << " Got max_P0_auto = " << maxP0_auto/1e6 << " MW" << endl;
  cout << " corresponding to average gradient = " << acs502.getVoltageLoaded(maxP0_auto,I_beam)/acs502.getL()/1e6 << "MV/m" << endl;
  cout << endl;

  /*
  //Check that return 0.0 works (comment out to test deltaT)
  max_Es /= 10;
  max_Sc /= 10;   //W/um^2, at P0 max is 4.6
  cout << "Finding maximum power at max_Es=" << max_Es << " MV/m, max_Sc=" << max_Sc << " W/um^2, Ibeam=" << I_beam << " A" << endl;
  //acs502.calc_g_integrals(1000); //Don't need to recalculate this all the time
  max_P0 = acs502.getMaxAllowablePower(I_beam, max_Es, max_Sc);
  cout << "Got max_P0 = " << max_P0/1e6 << " MW" << endl;
  acs502.writeProfileFile("/tmp/testfile_acs502_zeroPowerProfile.dat", max_P0, I_beam); //Update peak fields
  cout << "Es=" << acs502.getEs_max() << " MV/m, Sc_max=" << acs502.getSc_max() << " W/um^2" << endl;
  */

  //Get maximum beam time allowable
  double max_deltaT = AccelStructure::max_deltaT; //[K] TODO: Incorporate properly!
  bool useLoadedField = true;
  cout << "Finding maximum beam time, max deltaT=" << max_deltaT << " K, power = " << max_P0/1e6
       << " MW, I_beam = " << I_beam << " A, useLoadedField = " << (useLoadedField ? "TRUE" : "FALSE") << endl;
  cout << "This corresponds to loaded gradient " << acs502.getVoltageLoaded(max_P0, I_beam)/acs502.getL()/1e6 << " MV/m" << endl;
  double t_beam_max = acs502.getMaxAllowableBeamTime_dT(max_P0, I_beam, useLoadedField);
  cout << "Got t_beam_max = " << t_beam_max*1e9 << " ns" << endl;
  cout << endl;

  //Make a map over gradient and max fields/temperature as a function of power and time
  double min_P0 = acs502.getPowerLoaded(0.0,I_beam);
  size_t steps_P0 = 10;
  assert (steps_P0 >= 2);
  cout << "Mapping power between P_min = " << min_P0/1e6 << " MW (0 volts) and max_P0 = "
       << max_P0/1e6 << " MW in " << steps_P0 << " steps:" << endl;
  /*
    Plot this file in Gnuplot using:
    splot 'testfile_acs502_deltaTmap.dat' using 1:3:4 w p
    set xlabel 'Power [MW]'
    set ylabel 't_beam [cycles]'
    set zlabel 'deltaT [K]'
   */
  deltaTfile.open("/tmp/testfile_acs502_deltaTmap.dat");
  deltaTfile << "# power[MW] t_beam[ns] t_beam[cycles] deltaT[K]" << endl;

  for (size_t i_P0 = 0; i_P0 < steps_P0; i_P0++) {
    double power = (max_P0-min_P0)/(steps_P0-1)*i_P0 + min_P0;
    double voltage = acs502.getVoltageLoaded(power,I_beam);
    cout << "Now calculating power = " << power/1e6 << " MW, gradient = " << voltage/acs502.getL()/1e6 << " MV/m: ";

    //In case the power is so low there is a steady state solution with temperature rise < max_deltaT
    double t_beam_MAX = 50000*2*M_PI/acs502.getOmega();  // 50000 periods //TODO mod cycles?
    //TODO: Should be t_beam_MAX
    if (acs502.getMaxDeltaT(power, t_beam_max, I_beam, useLoadedField).maxDeltaT <= max_deltaT) {
      t_beam_max = t_beam_MAX;
      cout << "STEADY STATE SOLUTION for t_beam_max! ";
    }
    else {
      t_beam_max = acs502.getMaxAllowableBeamTime_dT(power, I_beam, useLoadedField);
    }
    cout << "Maximum beam time at this power = " << t_beam_max*1e9
         << " ns = " << t_beam_max*acs502.getOmega()/(2*M_PI) << " periods, for max_deltaT = "
         << (acs502.getMaxDeltaT(power, t_beam_max, I_beam, useLoadedField)).maxDeltaT << " K" << endl;
    stringstream fname;
    fname << "/tmp/testfile_acs502_deltaT_Pin" << power/1e6 << "_t_beam" << t_beam_max*1e9 << ".dat";
    acs502.writeDeltaTprofileFile(fname.str().c_str(), power, t_beam_max, I_beam, useLoadedField);

    size_t steps_t_beam = 10;
    for(size_t i_t_beam = 0; i_t_beam < steps_t_beam; i_t_beam++) {
      double t_beam = t_beam_max/(steps_t_beam - 1.0) * i_t_beam;
      //cout << "Now calculating at t_beam = " << t_beam*1e9 << " ns, "
      //     << "deltaT = " << (acs502.getMaxDeltaT(power, t_beam, I_beam, useLoadedField)).maxDeltaT
      //     << " K" << endl;
      deltaTfile << power/1e6 << " " << t_beam*1e9 << " " << t_beam*acs502.getOmega()/(2*M_PI)
                 << " " << (acs502.getMaxDeltaT(power, t_beam, I_beam, useLoadedField)).maxDeltaT << endl;
    }
  }
  deltaTfile.close();
  cout << endl;

  //Wakefield plot
  cout << "Plotting the wakefield..." << endl;
  acs502.writeWakeFile("/tmp/testfile_acs502_wakeFile.dat", 10.0, 0.0001);
  //Minimum bunch spacing (from paper: Limit 6.3 V/pC/mm/m)
  cout << "Minimum bunch spacing = " << acs502.getMinBunchSpacing(6.3) << " cycles" << endl;
  cout << endl;

  cout << endl;

  //Maximum times
  cout << "Maximum beam time [ns]: at P0 = "
       << P0/1e6 << " MW and I_beam = " << I_beam << " A:" << endl;
  cout << "\t Wasted beam pulse: " << acs502.getExtraTimePowerAboveFraction(P0, I_beam)*1e9 << endl;
  cout << "\t From Es:           " << acs502.getMaxAllowableBeamTime_E(P0,I_beam)*1e9 << endl;
  cout << "\t From Sc:           " << acs502.getMaxAllowableBeamTime_Sc(P0,I_beam)*1e9 << endl;
  cout << "\t From deltaT:       " << acs502.getMaxAllowableBeamTime_dT(P0,I_beam)*1e9 << endl;
  cout << "\t From P/C:          " << acs502.getMaxAllowableBeamTime_PC(P0,I_beam)*1e9 << endl;
  cout << "\t Overall:           " << acs502.getMaxAllowableBeamTime(P0,I_beam)*1e9 << endl;
  cout << "\t Overall, detailed: " << acs502.getMaxAllowableBeamTime_detailed(P0,I_beam) << endl;
  cout << endl << endl << endl;

  //Testing the CLIC_G structure
  I_beam = 1.60217646e-19*3.7e9/0.5e-9; //[A]
  cout << "Setting I_beam = " << I_beam << " [A]" << endl;

  cout << "Creating an AccelStructure_CLICG (R05 variety, no database)" << endl;
  //AccelStructure_CLICG acsG_R05(24, true);
  AccelStructure_CLICG acsG_R05(26, true);
  acsG_R05.calc_g_integrals(500);
  cout << "t_fill = " << acsG_R05.getTfill()*1e9 << "[ns]" << endl;
  cout << "t_rise = " << acsG_R05.getTrise()*1e9 << "[ns]" << endl;

  cout << "FIRST = " << acsG_R05.getCellFirst() << endl;
  cout << "MID   = " << acsG_R05.getCellMid()   << endl;
  cout << "LAST  = " << acsG_R05.getCellLast()  << endl;

  cout << "V24 = " << acsG_R05.getVoltageUnloaded(1.0) << " Volts (expected 3080 V)" << endl;
  cout << "Pin at 100 MV/m = "
       << acsG_R05.getPowerUnloaded(100e6*acsG_R05.getL())/1e6 << " MW (expected 42.1 MW) (unloaded)" << endl;
  cout << "Peak fields (unloaded): " << acsG_R05.getMaxFields(acsG_R05.getPowerUnloaded(100e6*acsG_R05.getL())) << endl;

  P0 = acsG_R05.getPowerLoaded(100e6*acsG_R05.getL(), I_beam);
  cout << "Pin at 100 MV/m = " << P0  << " MW" << endl;
  cout << "Peak fields (loaded): " << acsG_R05.getMaxFields(P0, I_beam) << endl;

  cout << "Writing power profile file" << endl;
  acsG_R05.writeTimePowerProfileFile("/tmp/testfile_acsG_R05_timePowerProfile.dat", P0, 312*0.5e-9, I_beam, 500);
  acsG_R05.writeTimeDeltaTprofileFile("/tmp/testfile_acsG_R05_timeDeltaTprofile.dat", P0, 4*312*0.5e-9, I_beam, false, 2*4*312);

  cout << "Writing parameter profile file" << endl;
  acsG_R05.writeParameterProfileFile("/tmp/testfile_acsG_R05_parameterProfile.dat");

  cout << "Writing wake file" << endl;
  acsG_R05.writeWakeFile("/tmp/testfile_acsG_R05_wakeFile.dat", 10.0, 0.0001);
  cout << "Minimum bunch spacing = " << acsG_R05.getMinBunchSpacing(6.6) << " cycles" << endl;

  cout << "Writing profile files, gradient scan" << endl;
  for (double G = 10.0; G <= 120.0; G += 10) {
    cout << "Now at loaded G = " << G << " MV/m, ";
    P0 = acsG_R05.getPowerLoaded(G*1e6*acsG_R05.getL(), I_beam);
    cout << "P0 = " << P0/1e6 << " MW, corresponding to unloaded G = ";
    double Gul = acsG_R05.getVoltageUnloaded(P0)/acsG_R05.getL() / 1e6; //MV/m
    cout << Gul << " MV/m" << endl;

    stringstream fname;
    fname << "/tmp/testfile_acsG_R05_unloaded_G=" << Gul << "MVm.dat";
    cout << "\t Writing file " << fname.str() << endl;
    acsG_R05.writeProfileFile(fname.str().c_str(), P0,0.0);

    fname.str(""); fname.clear();

    fname << "/tmp/testfile_acsG_R05_loaded_G=" << G << "MVm.dat";
    cout << "\t Writing file " << fname.str() << endl;
    acsG_R05.writeProfileFile(fname.str().c_str(), P0,I_beam);

    cout << "Max allowable beam time ( loaded   )          : "
         << acsG_R05.getMaxAllowableBeamTime_detailed(P0, I_beam) << endl;
    cout << "Max allowable beam time ( unloaded )          : "
         << acsG_R05.getMaxAllowableBeamTime_detailed(P0, 0.0) << endl;
    cout << "Max allowable beam time ( loaded pulse, I=0 ) : "
         << acsG_R05.getMaxAllowableBeamTime_detailed(P0, I_beam, 0.0) << endl;
    cout << endl << endl;
  }

  cout << "Writing profile files, current scan at G_L = 100 MV/m" << endl;
  for (double I = 0.0; I <= 2.1; I += 0.25) {
    cout << "Now at I = " << I << " A" << endl;
    double G = 100;
    P0 = acsG_R05.getPowerLoaded(G*1e6*acsG_R05.getL(), I);
    cout << "P0 = " << P0/1e6 << " MW, corresponding to unloaded G = ";
    double Gul = acsG_R05.getVoltageUnloaded(P0)/acsG_R05.getL() / 1e6; //MV/m
    cout << Gul << " MV/m" << endl;

    stringstream fname;
    fname << "/tmp/testfile_acsG_R05_loaded_G=" << G << "MVm_I=" << I << ".dat";
    cout << "\t Writing file " << fname.str() << endl;
    acsG_R05.writeProfileFile(fname.str().c_str(), P0,I);

    cout << "Max allowable beam time ( loaded   )          : "
         << acsG_R05.getMaxAllowableBeamTime_detailed(P0, I) << endl;
    cout << "Max allowable beam time ( unloaded )          : "
         << acsG_R05.getMaxAllowableBeamTime_detailed(P0, 0.0) << endl;
    cout << "Max allowable beam time ( loaded pulse, I=0 ) : "
         << acsG_R05.getMaxAllowableBeamTime_detailed(P0, I, 0.0) << endl;
    cout << endl << endl;

  }

  //Check for pulse length wrt experiment
  cout << "pulse length check WRT experiment..." << endl;
  double acsG_R05__P0 = acsG_R05.getPowerUnloaded( 102.5*1e6 * acsG_R05.getL() );

  return_AccelStructure_getMaxAllowableBeamTime_detailed
    acsG_R05__tbeam = acsG_R05.getMaxAllowableBeamTime_detailed(acsG_R05__P0, 0.0);
  cout << acsG_R05__tbeam << endl;

  double acsG_R05__tE  = (acsG_R05__tbeam.time_E  + acsG_R05__tbeam.wastedTime)*1e9;
  double acsG_R05__tSc = (acsG_R05__tbeam.time_Sc + acsG_R05__tbeam.wastedTime)*1e9;
  double acsG_R05__tPC = (acsG_R05__tbeam.time_PC + acsG_R05__tbeam.wastedTime)*1e9;
  double acsG_R05__tau = min(min(acsG_R05__tE, acsG_R05__tSc), acsG_R05__tPC);
  if ((acsG_R05__tbeam.time_dT + acsG_R05__tbeam.wastedTime)*1e9 < acsG_R05__tau) {
    cout << "Warning, time_dT = " << acsG_R05__tbeam.time_dT*1e9 << " ns" << endl;
  }

  //BDR was 2e6 bpp -- scale pulselengths by (BDR/BDR0)^(1/5)
  cout << "tau   = " << acsG_R05__tau << " ns, scaled to 2e-6 -> "
       << acsG_R05__tau * pow(2e-6/1.0e-6, 1.0/5.0) << endl
       << "tauE  = " << acsG_R05__tE  << " ns, scaled to 2e-6 -> "
       << acsG_R05__tE * pow(2e-6/1.0e-6, 1.0/5.0) << endl
       << "tauSc = " << acsG_R05__tSc << " ns, scaled to 2e-6 -> "
       << acsG_R05__tSc * pow(2e-6/1.0e-6, 1.0/5.0) << endl
       << "tauPC = " << acsG_R05__tPC << " ns, scaled to 2e-6 -> "
       << acsG_R05__tPC * pow(2e-6/1.0e-6, 1.0/5.0) << endl
       << "taudT = " << (acsG_R05__tbeam.time_dT + acsG_R05__tbeam.wastedTime)*1e9 << " ns" << endl;
  cout << endl;

  //Creating an AccelStructure\_general which is a copy of acsG_R05
  cout << "Creating a a copy of the AccelStructure_CLICG (R05 variety, no database)" << endl;
  AccelStructure_general acsG_R05_copy = AccelStructure_general::copy_structure(acsG_R05,"AccelStructure_general copy of acsG_R05");
  acsG_R05_copy.calc_g_integrals(500);
  cout << "L=" <<acsG_R05_copy.getL() << "m";
  cout << "t_fill = " << acsG_R05_copy.getTfill()*1e9 << "[ns]" << endl;
  cout << "t_rise = " << acsG_R05_copy.getTrise()*1e9 << "[ns]" << endl;

  cout << "FIRST = " << acsG_R05_copy.getCellFirst() << endl;
  cout << "MID   = " << acsG_R05_copy.getCellMid()   << endl;
  cout << "LAST  = " << acsG_R05_copy.getCellLast()  << endl;

  cout << "V24 = " << acsG_R05_copy.getVoltageUnloaded(1.0) << " Volts (expected 3080 V)" << endl;
  cout << "Pin at 100 MV/m = "
       << acsG_R05_copy.getPowerUnloaded(100e6*acsG_R05_copy.getL())/1e6 << " MW (expected 42.1 MW) (unloaded)" << endl;
  cout << "Peak fields (unloaded): "
       << acsG_R05_copy.getMaxFields(acsG_R05_copy.getPowerUnloaded(100e6*acsG_R05_copy.getL())) << endl;

  P0 = acsG_R05_copy.getPowerLoaded(100e6*acsG_R05_copy.getL(), I_beam);
  cout << "Pin at 100 MV/m = " << P0  << " MW" << endl;
  cout << "Peak fields (loaded): " << acsG_R05_copy.getMaxFields(P0, I_beam) << endl;

  cout << "Writing parameter profile file" << endl;
  acsG_R05_copy.writeParameterProfileFile("/tmp/testfile_acsG_R05_copy_parameterProfile.dat");

  cout << endl << endl << endl;

  // Creating acsG, no DB
  cout << "Creating an AccelStructure_CLICG (normal variety, no database)" << endl;
  AccelStructure_CLICG acsG(24, false);
  acsG.calc_g_integrals(500);
  cout << "t_fill = " << acsG.getTfill()*1e9 << "[ns]" << endl;

  cout << "FIRST = " << acsG.getCellFirst() << endl;
  cout << "MID   = " << acsG.getCellMid()   << endl;
  cout << "LAST  = " << acsG.getCellLast()  << endl;

  cout << "V24 = " << acsG.getVoltageUnloaded(1.0) << " Volts (expected 3078 V)" << endl;
  cout << "Pin at 100 MV/m = " << acsG.getPowerUnloaded(100e6*acsG.getL())/1e6 << " MW (expected 42.2 MW) (unloaded)" << endl;
  cout << "Peak fields (unloaded): " << acsG.getMaxFields(acsG.getPowerUnloaded(100e6*acsG.getL())) << endl;

  cout << "Pin at 100 MV/m = " << acsG.getPowerLoaded(100e6*acsG.getL(), I_beam) << " MW" << endl;
  cout << "Peak fields (loaded): " << acsG.getMaxFields(acsG.getPowerLoaded(100e6*acsG.getL(), I_beam)) << endl;

  cout << "Writing profile files" << endl;

  P0 = acsG.getPowerLoaded(100e6*acsG.getL(), I_beam);
  cout << "P0 = " << P0/1e6 << " MW" << endl;
  acsG.writeProfileFile("/tmp/testfile_acsG_unloaded.dat", P0,0.0);
  acsG.writeProfileFile("/tmp/testfile_acsG_loaded.dat", P0,I_beam);
  cout << "Max allowable beam time (loaded): " << acsG.getMaxAllowableBeamTime_detailed(P0, I_beam) << endl;
  cout << endl << endl;



  // TD24R05, KEK frequency
  cout << "CLIC_G / R05 / 24 cells / 11.424 GHz" << endl;
  CellBase_compat cBase11424 ("cellBase/TD_12GHz_v2.dat",11.424,false,3);
  CellParams KEK_Gfirst = CellParams(acsG_R05.getCellFirst()); cBase11424.scaleCell(KEK_Gfirst);
  CellParams KEK_Gmid   = CellParams(acsG_R05.getCellMid())  ; cBase11424.scaleCell(KEK_Gmid);
  CellParams KEK_Glast  = CellParams(acsG_R05.getCellLast()) ; cBase11424.scaleCell(KEK_Glast);
  double h11424 = (KEK_Gfirst.h + KEK_Gmid.h + KEK_Glast.h) / 3.0;
  KEK_Gfirst.h = h11424; KEK_Gmid.h = h11424; KEK_Glast.h = h11424;
  //cout <<  "h11424 = " << h11424*1e3 << " mm" << endl;

  AccelStructure_general acsG_R05_24_KEK(24, KEK_Gfirst,KEK_Gmid,KEK_Glast);
  acsG_R05_24_KEK.calc_g_integrals(500);
  //Unloaded gradient was 100 MV/m
  double acsG_R05_24_KEK__P0 = acsG_R05_24_KEK.getPowerUnloaded( 100.0*1e6 * acsG_R05_24_KEK.getL() );

  return_AccelStructure_getMaxAllowableBeamTime_detailed
    acsG_R05_24_KEK__tbeam = acsG_R05_24_KEK.getMaxAllowableBeamTime_detailed(acsG_R05_24_KEK__P0, 0.0);
  cout << acsG_R05_24_KEK__tbeam << endl;

  double acsG_R05_24_KEK__tE  = (acsG_R05_24_KEK__tbeam.time_E  + acsG_R05_24_KEK__tbeam.wastedTime)*1e9;
  double acsG_R05_24_KEK__tSc = (acsG_R05_24_KEK__tbeam.time_Sc + acsG_R05_24_KEK__tbeam.wastedTime)*1e9;
  double acsG_R05_24_KEK__tPC = (acsG_R05_24_KEK__tbeam.time_PC + acsG_R05_24_KEK__tbeam.wastedTime)*1e9;
  double acsG_R05_24_KEK__tau = min(min(acsG_R05_24_KEK__tE, acsG_R05_24_KEK__tSc), acsG_R05_24_KEK__tPC);
  if ((acsG_R05_24_KEK__tbeam.time_dT + acsG_R05_24_KEK__tbeam.wastedTime)*1e9 < acsG_R05_24_KEK__tau) {
    cout << "Warning, time_dT = " << acsG_R05_24_KEK__tbeam.time_dT*1e9 << " ns" << endl;
  }

  //BDR was 1.1e6 bpp -- scale pulselengths by (BDR/BDR0)^(1/5)
  cout << "tau   = " << acsG_R05_24_KEK__tau << " ns, scaled to 1.1e-6 -> "
       << acsG_R05_24_KEK__tau * pow(1.1e-6/1.0e-6, 1.0/5.0) << endl
       << "tauE  = " << acsG_R05_24_KEK__tE  << " ns, scaled to 1.1e-6 -> "
       << acsG_R05_24_KEK__tE * pow(1.1e-6/1.0e-6, 1.0/5.0) << endl
       << "tauSc = " << acsG_R05_24_KEK__tSc << " ns, scaled to 1.1e-6 -> "
       << acsG_R05_24_KEK__tSc * pow(1.1e-6/1.0e-6, 1.0/5.0) << endl
       << "tauPC = " << acsG_R05_24_KEK__tPC << " ns, scaled to 1.1e-6 -> "
       << acsG_R05_24_KEK__tPC * pow(1.1e-6/1.0e-6, 1.0/5.0) << endl
       << "taudT = " << (acsG_R05_24_KEK__tbeam.time_dT + acsG_R05_24_KEK__tbeam.wastedTime)*1e9 << " ns" << endl;
  cout << endl;

  /// Database structures ///
  cout << "STANDARD CLIC_G as in the CDR:" << endl;
  I_beam = 1.60217646e-19*3.72e9/0.5e-9; //[A]
  cout << "Setting I_beam = " << I_beam << " [A]" << endl;
  int Ncells = 24;
  cout << "Setting Ncells = " << Ncells << endl;
  cout << endl;

  //Testing Paramset2 with CellBase_linearInterpolation_freqScaling (same cell parameters as acsG)
  cout << "Creating a paramset2 structure using CellBase_linearInterpolation_freqScaling, CLIC_G parameters" << endl;
  CellBase_linearInterpolation_freqScaling base2("cellBase/TD_30GHz.dat", 3, offsets, 11.9942);
  /*
  AccelStructure_paramSet2 acs2_li(&base2,
                                   24, 120.0,
                                   0.110022947942206, 0.016003337882503*2,
                                   0.160233420548558, 0.040208386429788*2);
  */
  /*
  AccelStructure_paramSet2 acs2_li(&base2,
                                   24, 120.0,
                                   0.110023, 0.0160034*2,
                                   0.160226, 0.040206*2);
  */

  //Use the mm measurements
  AccelStructure_paramSet2 acs2_li(&base2,
                                   Ncells, 120.0,
                                   0.110022947942206, 0.016003337882503*2,
                                   0.160233420548558, 0.040208386429788*2);

  cout << "Length = " << acs2_li.getL()*1e3 << " mm" << endl;

  acs2_li.calc_g_integrals(500);
  cout << "t_fill = " << acs2_li.getTfill()*1e9 << "[ns]" << endl;
  cout << "t_rise = " << acs2_li.getTrise()*1e9 << "[ns]" << endl;

  cout << "FIRST = " << acs2_li.getCellFirst() << endl;
  cout << "MID   = " << acs2_li.getCellMid()   << endl;
  cout << "LAST  = " << acs2_li.getCellLast()  << endl;

  //cout << "V24 = " << acs2_li.getVoltageUnloaded(1.0) << " Volts" << endl;
  cout << "Pin at 100 MV/m = " << acs2_li.getPowerUnloaded(100e6*acs2_li.getL())/1e6 << " MW" << endl;
  cout << "Peak fields (unloaded): " << acs2_li.getMaxFields(acs2_li.getPowerUnloaded(100e6*acs2_li.getL())) << endl;

  P0 = acs2_li.getPowerLoaded(100e6*acs2_li.getL(), I_beam);
  cout << "Pin at 100 MV/m = " << P0/1e6 << " MW (loaded)" << endl;
  cout << "Peak fields (loaded): " << acs2_li.getMaxFields(P0, I_beam) << endl;

  cout << "Max beam time (loaded): " << acs2_li.getMaxAllowableBeamTime_detailed(P0, I_beam) << endl;
  cout << "Max beam time (loaded pulse, no beam): " << acs2_li.getMaxAllowableBeamTime_detailed(P0, I_beam, 0.0) << endl;
  cout << "Energy to beam = " << 1.60217646e-19*3.72e9*312 * acs2_li.getVoltageLoaded(P0, I_beam) << " J" << endl;
  cout << "Energy to structure = " << P0 * (acs2_li.getTfill()+ acs2_li.getTrise() + 312*0.5e-9) << " J" << endl;
  cout << "RF -> beam efficiency = " << acs2_li.getTotalEfficiency(P0, I_beam, 312*0.5e-9)*100 << " %" << endl;

  cout << "Min bunch spacing (6.6 V/pC/mm/m) " << acs2_li.getMinBunchSpacing(6.6) << " cycles" <<endl;

  cout << endl;

  //Testing Paramset2 with CellBase_compat
  cout << "Creating a paramset2 structure using CellBase_compat" << endl;
  CellBase_compat base3("cellBase/TD_30GHz.dat", 11.9942);
  //Use the mm measurements. 24 cells to get acsG.
  AccelStructure_paramSet2 acs2(&base3,
                                Ncells, 120.0,
                                0.110022947942206, 0.016003337882503*2,
                                0.160233420548558, 0.040208386429788*2);
  cout << "Length = "
       << acs2.getL()*1e3 << " mm" << endl;
  acs2.calc_g_integrals(500);
  cout << "t_rise = "
       << acs2.getTrise()*1e9 << "[ns]" << endl;
  cout << "t_fill = "
       << acs2.getTfill()*1e9 << "[ns]" << endl;

  cout << "FIRST = "
       << acs2.getCellFirst() << endl;
  cout << "MID   = "
       << acs2.getCellMid()   << endl;
  cout << "LAST  = "
       << acs2.getCellLast()  << endl;

  //cout << "V = " << acs2.getVoltageUnloaded(1.0) << " Volts" << endl;
  cout << "Pin at 100 MV/m = "
       << acs2.getPowerUnloaded(100e6*acs2.getL())/1e6 << " MW" << endl;
  cout << "Peak fields (unloaded): "
       << acs2.getMaxFields(acs2.getPowerUnloaded(100e6*acs2.getL())) << endl;

  P0 = acs2.getPowerLoaded(100e6*acs2.getL(), I_beam);
  cout << "Pin at 100 MV/m = "
       << P0/1e6 << " MW (loaded)" << endl;
  cout << "Peak fields (loaded): "
       << acs2.getMaxFields(P0, I_beam) << endl;

  cout << "Max beam time (loaded): "
       << acs2.getMaxAllowableBeamTime_detailed(P0, I_beam) << endl;
  cout << "Max beam time (loaded pulse, no beam): "
       << acs2.getMaxAllowableBeamTime_detailed(P0, I_beam, 0.0) << endl;
  cout << "Max beam time (loaded power, I = 0): "
       << acs2.getMaxAllowableBeamTime_detailed(P0, 0.0) << endl;
  cout << "Max beam time (P(G_UL = 100 MV/m), I = 0): "
       << acs2.getMaxAllowableBeamTime_detailed(acs2.getPowerUnloaded(100e6*acs2.getL()), 0.0) << endl;
  cout << "Energy to beam = "
       << 1.60217646e-19*3.72e9*312 * acs2.getVoltageLoaded(P0, I_beam) << " J" << endl;
  cout << "Energy to structure = "
       << P0 * (acs2.getTfill()+ acs2.getTrise() + 312*0.5e-9) << " J" << endl;
  cout << "RF -> beam efficiency = "
       << acs2.getTotalEfficiency(P0, I_beam, 312*0.5e-9)*100 << " %" << endl;

  cout << "Min bunch spacing (6.6 V/pC/mm/m) " << acs2.getMinBunchSpacing(6.6) << " cycles" <<endl;

  cout << "Plotting the wakefield..." << endl;
  acs2.writeWakeFile("/tmp/testfile_acs2_wakeFile.dat", 10.0, 0.0001);

  cout << "Plotting pulse shapes..." << endl;
  acs2.writeTimePowerProfileFile("/tmp/testfile_acs2_timePowerProfile.dat", P0, 312*0.5e-9, I_beam, 500);
  acs2.writeTimePowerProfileFile("/tmp/testfile_acs2_timePowerProfile_nobeam.dat", P0, 312*0.5e-9, 0.0, 500);

  cout << "Plotting field profiles..." << endl;
  acs2.writeProfileFile("/tmp/testfile_acs2_fieldprofile_loaded.dat", P0, I_beam);
  acs2.writeProfileFile("/tmp/testfile_acs2_fieldprofile_unloaded.dat", P0, 0.0);
  acs2.writeDeltaTprofileFile("/tmp/testfile_acs2_deltaTprofile_loaded.dat", P0, 312*0.5e-9, I_beam,true);
  acs2.writeDeltaTprofileFile("/tmp/testfile_acs2_deltaTprofile_unloaded.dat", P0, 312*0.5e-9, I_beam,false);

  cout << endl;

  //Testing Paramset2 with CellBase_compat, new DB v1
  cout << "Creating a paramset2_noPsi structure using CellBase_compat and 12GHz database / version 1" << endl;
  CellBase_compat base4("cellBase/TD_12GHz_v1.dat", 11.9942, false, 2);

  //Use the mm measurements. 24 cells to get acsG.
  AccelStructure_paramSet2_noPsi acs2_DB12v1(&base4,
                                             Ncells, 0.110022947942206, 0.016003337882503*2,
                                             0.160233420548558, 0.040208386429788*2);
  cout << "Length = "
       << acs2_DB12v1.getL()*1e3 << " mm" << endl;
  acs2_DB12v1.calc_g_integrals(500);
  cout << "t_rise = "
       << acs2_DB12v1.getTrise()*1e9 << "[ns]" << endl;
  cout << "t_fill = "
       << acs2_DB12v1.getTfill()*1e9 << "[ns]" << endl;

  cout << "FIRST = "
       << acs2_DB12v1.getCellFirst() << endl;
  cout << "MID   = "
       << acs2_DB12v1.getCellMid()   << endl;
  cout << "LAST  = "
       << acs2_DB12v1.getCellLast()  << endl;

  //cout << "V = " << acs2.getVoltageUnloaded(1.0) << " Volts" << endl;
  cout << "Pin at 100 MV/m = "
       << acs2_DB12v1.getPowerUnloaded(100e6*acs2_DB12v1.getL())/1e6 << " MW" << endl;
  cout << "Peak fields (unloaded): "
       << acs2_DB12v1.getMaxFields(acs2_DB12v1.getPowerUnloaded(100e6*acs2_DB12v1.getL())) << endl;

  P0 = acs2_DB12v1.getPowerLoaded(100e6*acs2_DB12v1.getL(), I_beam);
  cout << "Pin at 100 MV/m = "
       << P0/1e6 << " MW (loaded)" << endl;
  cout << "Peak fields (loaded): "
       << acs2_DB12v1.getMaxFields(P0, I_beam) << endl;

  cout << "Max beam time (loaded): "
       << acs2_DB12v1.getMaxAllowableBeamTime_detailed(P0, I_beam) << endl;
  cout << "Max beam time (loaded pulse, no beam): "
       << acs2_DB12v1.getMaxAllowableBeamTime_detailed(P0, I_beam, 0.0) << endl;
  cout << "Max beam time (loaded power, I = 0): "
       << acs2_DB12v1.getMaxAllowableBeamTime_detailed(P0, 0.0) << endl;
  cout << "Max beam time (P(G_UL = 100 MV/m), I = 0): "
       << acs2_DB12v1.getMaxAllowableBeamTime_detailed(acs2_DB12v1.getPowerUnloaded(100e6*acs2_DB12v1.getL()), 0.0) << endl;
  cout << "Energy to beam = "
       << 1.60217646e-19*3.72e9*312 * acs2_DB12v1.getVoltageLoaded(P0, I_beam) << " J" << endl;
  cout << "Energy to structure = "
       << P0 * (acs2_DB12v1.getTfill()+ acs2_DB12v1.getTrise() + 312*0.5e-9) << " J" << endl;
  cout << "RF -> beam efficiency = "
       << acs2_DB12v1.getTotalEfficiency(P0, I_beam, 312*0.5e-9)*100 << " %" << endl;

  cout << "Min bunch spacing (6.6 V/pC/mm/m) " << acs2_DB12v1.getMinBunchSpacing(6.6) << " cycles" <<endl;

  cout << "Plotting the wakefield..." << endl;
  acs2_DB12v1.writeWakeFile("/tmp/testfile_acs2_DB12v1_wakeFile.dat", 10.0, 0.0001);

  cout << "Plotting pulse shapes..." << endl;
  acs2_DB12v1.writeTimePowerProfileFile("/tmp/testfile_acs2_DB12v1_timePowerProfile.dat", P0, 312*0.5e-9, I_beam, 500);
  acs2_DB12v1.writeTimePowerProfileFile("/tmp/testfile_acs2_DB12v1_timePowerProfile_nobeam.dat", P0, 312*0.5e-9, 0.0, 500);

  cout << "Plotting field profiles..." << endl;
  acs2_DB12v1.writeProfileFile("/tmp/testfile_acs2_DB12v1_fieldprofile_loaded.dat", P0, I_beam);
  acs2_DB12v1.writeProfileFile("/tmp/testfile_acs2_DB12v1_fieldprofile_unloaded.dat", P0, 0.0);
  acs2_DB12v1.writeDeltaTprofileFile("/tmp/testfile_acs2_DB12v1_deltaTprofile_loaded.dat", P0, 312*0.5e-9, I_beam,true);
  acs2_DB12v1.writeDeltaTprofileFile("/tmp/testfile_acs2_DB12v1_deltaTprofile_unloaded.dat", P0, 312*0.5e-9, I_beam,false);

  cout << endl;

  //Testing Paramset2 with CellBase_compat, new DB v2
  cout << "Creating a paramset2_noPsi structure using CellBase_compat  12GHz database / version 2" << endl;
  CellBase_compat base5("cellBase/TD_12GHz_v2.dat", 11.9942, false, 2);

  //Use the mm measurements. 24 cells to get acsG.
  AccelStructure_paramSet2_noPsi acs2_DB12v2(&base5,
                                             Ncells,
                                             0.110022947942206, 0.016003337882503*2,
                                             0.160233420548558, 0.040208386429788*2);

  cout << "Length = "
       << acs2_DB12v2.getL()*1e3 << " mm" << endl;
  acs2_DB12v2.calc_g_integrals(500);
  cout << "t_rise = "
       << acs2_DB12v2.getTrise()*1e9 << "[ns]" << endl;
  cout << "t_fill = "
       << acs2_DB12v2.getTfill()*1e9 << "[ns]" << endl;

  cout << "FIRST = "
       << acs2_DB12v2.getCellFirst() << endl;
  cout << "MID   = "
       << acs2_DB12v2.getCellMid()   << endl;
  cout << "LAST  = "
       << acs2_DB12v2.getCellLast()  << endl;

  //cout << "V = " << acs2.getVoltageUnloaded(1.0) << " Volts" << endl;
  cout << "Pin at 100 MV/m = "
       << acs2_DB12v2.getPowerUnloaded(100e6*acs2_DB12v2.getL())/1e6 << " MW" << endl;
  cout << "Peak fields (unloaded): "
       << acs2_DB12v2.getMaxFields(acs2_DB12v2.getPowerUnloaded(100e6*acs2_DB12v2.getL())) << endl;

  P0 = acs2_DB12v2.getPowerLoaded(100e6*acs2_DB12v2.getL(), I_beam);
  cout << "Pin at 100 MV/m = "
       << P0/1e6 << " MW (loaded)" << endl;
  cout << "Peak fields (loaded): "
       << acs2_DB12v2.getMaxFields(P0, I_beam) << endl;

  cout << "Max beam time (loaded): "
       << acs2_DB12v2.getMaxAllowableBeamTime_detailed(P0, I_beam) << endl;
  cout << "Max beam time (loaded pulse, no beam): "
       << acs2_DB12v2.getMaxAllowableBeamTime_detailed(P0, I_beam, 0.0) << endl;
  cout << "Max beam time (loaded power, I = 0): "
       << acs2_DB12v2.getMaxAllowableBeamTime_detailed(P0, 0.0) << endl;
  cout << "Max beam time (P(G_UL = 100 MV/m), I = 0): "
       << acs2_DB12v2.getMaxAllowableBeamTime_detailed(acs2_DB12v2.getPowerUnloaded(100e6*acs2_DB12v2.getL()), 0.0) << endl;
  cout << "Energy to beam = "
       << 1.60217646e-19*3.72e9*312 * acs2_DB12v2.getVoltageLoaded(P0, I_beam) << " J" << endl;
  cout << "Energy to structure = "
       << P0 * (acs2_DB12v2.getTfill()+ acs2_DB12v2.getTrise() + 312*0.5e-9) << " J" << endl;
  cout << "RF -> beam efficiency = "
       << acs2_DB12v2.getTotalEfficiency(P0, I_beam, 312*0.5e-9)*100 << " %" << endl;

  cout << "Min bunch spacing (6.6 V/pC/mm/m) " << acs2_DB12v2.getMinBunchSpacing(6.6) << " cycles" <<endl;

  cout << "Plotting the wakefield..." << endl;
  acs2_DB12v2.writeWakeFile("/tmp/testfile_acs2_DB12v2_wakeFile.dat", 10.0, 0.0001);

  cout << "Plotting pulse shapes..." << endl;
  acs2_DB12v2.writeTimePowerProfileFile("/tmp/testfile_acs2_DB12v2_timePowerProfile.dat", P0, 312*0.5e-9, I_beam, 500);
  acs2_DB12v2.writeTimePowerProfileFile("/tmp/testfile_acs2_DB12v2_timePowerProfile_nobeam.dat", P0, 312*0.5e-9, 0.0, 500);

  cout << "plotting pulse heating time profiles" << endl;
  acs2_DB12v2.writeTimeDeltaTprofileFile("/tmp/testfile_acs2_DB12v2_timeDeltaTprofile.dat", P0, 2*312*0.5e-9, I_beam, true, 500);
  acs2_DB12v2.writeTimeDeltaTprofileFile("/tmp/testfile_acs2_DB12v2_timeDeltaTprofile_noBeam.dat",
                                         P0, 2*312*0.5e-9, I_beam, false, 500);

  cout << "Plotting field profiles..." << endl;
  acs2_DB12v2.writeProfileFile("/tmp/testfile_acs2_DB12v2_fieldprofile_loaded.dat", P0, I_beam);
  acs2_DB12v2.writeProfileFile("/tmp/testfile_acs2_DB12v2_fieldprofile_unloaded.dat", P0, 0.0);
  acs2_DB12v2.writeDeltaTprofileFile("/tmp/testfile_acs2_DB12v2_deltaTprofile_loaded.dat", P0, 312*0.5e-9, I_beam, true);
  acs2_DB12v2.writeDeltaTprofileFile("/tmp/testfile_acs2_DB12v2_deltaTprofile_unloaded.dat", P0, 312*0.5e-9, I_beam, false);

  cout << endl;

  //Special tests for Daniel
  cout << "Inverted iris thickness, cellBase_compat:" << endl;

  //Use the mm measurements
  AccelStructure_paramSet2 acs2_invIrisThick(&base3,
                                             Ncells, 120.0,
                                             0.110022947942206,  0.016003337882503*2,
                                             0.160233420548558, -0.040208386429788*2);

  cout << "Length = "
       << acs2_invIrisThick.getL()*1e3 << " mm" << endl;
  acs2_invIrisThick.calc_g_integrals(500);
  cout << "t_rise = "
       << acs2_invIrisThick.getTrise()*1e9 << "[ns]" << endl;
  cout << "t_fill = "
       << acs2_invIrisThick.getTfill()*1e9 << "[ns]" << endl;

  cout << "FIRST = "
       << acs2_invIrisThick.getCellFirst() << endl;
  cout << "MID   = "
       << acs2_invIrisThick.getCellMid()   << endl;
  cout << "LAST  = "
       << acs2_invIrisThick.getCellLast()  << endl;

  //cout << "V = " << acs2_invIrisThick.getVoltageUnloaded(1.0) << " Volts" << endl;
  cout << "Pin at 100 MV/m = "
       << acs2_invIrisThick.getPowerUnloaded(100e6*acs2_invIrisThick.getL())/1e6 << " MW" << endl;
  cout << "Peak fields (unloaded): "
       << acs2_invIrisThick.getMaxFields(acs2_invIrisThick.getPowerUnloaded(100e6*acs2_invIrisThick.getL())) << endl;

  P0 = acs2_invIrisThick.getPowerLoaded(100e6*acs2_invIrisThick.getL(), I_beam);
  cout << "Pin at 100 MV/m = "
       << P0/1e6 << " MW (loaded)" << endl;
  cout << "Peak fields (loaded): "
       << acs2_invIrisThick.getMaxFields(P0, I_beam) << endl;

  cout << "Max beam time (loaded): "
       << acs2_invIrisThick.getMaxAllowableBeamTime_detailed(P0, I_beam) << endl;
  cout << "Max beam time (loaded pulse, no beam): "
       << acs2_invIrisThick.getMaxAllowableBeamTime_detailed(P0, I_beam, 0.0) << endl;
  cout << "Energy to beam = "
       << 1.60217646e-19*3.72e9*312 * acs2_invIrisThick.getVoltageLoaded(P0, I_beam) << " J" << endl;
  cout << "Energy to structure = "
       << P0 * (acs2_invIrisThick.getTfill()+ acs2_invIrisThick.getTrise() + 312*0.5e-9) << " J" << endl;
  cout << "RF -> beam efficiency = "
       << acs2_invIrisThick.getTotalEfficiency(P0, I_beam, 312*0.5e-9)*100 << " %" << endl;

  cout << "Min bunch spacing (6.6 V/pC/mm/m) "
       << acs2_invIrisThick.getMinBunchSpacing(6.6) << " cycles" <<endl;

  cout << endl;

  cout << "No iris thickness tapering, cellBase_compat:" << endl;

  //Use the mm measurements
  AccelStructure_paramSet2 acs2_noIrisThickTaper(&base3,
                                                 Ncells, 120.0,
                                                 0.110022947942206, 0.016003337882503*2,
                                                 0.160233420548558, 0.0);

  cout << "Length = "
       << acs2_noIrisThickTaper.getL()*1e3 << " mm" << endl;
  acs2_noIrisThickTaper.calc_g_integrals(500);
  cout << "t_rise = "
       << acs2_noIrisThickTaper.getTrise()*1e9 << "[ns]" << endl;
  cout << "t_fill = "
       << acs2_noIrisThickTaper.getTfill()*1e9 << "[ns]" << endl;

  cout << "FIRST = "
       << acs2_noIrisThickTaper.getCellFirst() << endl;
  cout << "MID   = "
       << acs2_noIrisThickTaper.getCellMid()   << endl;
  cout << "LAST  = "
       << acs2_noIrisThickTaper.getCellLast()  << endl;

  //cout << "V = " << acs2_noIrisThickTaper.getVoltageUnloaded(1.0) << " Volts" << endl;
  cout << "Pin at 100 MV/m = "
       << acs2_noIrisThickTaper.getPowerUnloaded(100e6*acs2_noIrisThickTaper.getL())/1e6 << " MW" << endl;
  cout << "Peak fields (unloaded): "
       << acs2_noIrisThickTaper.getMaxFields(acs2_noIrisThickTaper.getPowerUnloaded(100e6*acs2_noIrisThickTaper.getL())) << endl;

  P0 = acs2_noIrisThickTaper.getPowerLoaded(100e6*acs2_noIrisThickTaper.getL(), I_beam);
  cout << "Pin at 100 MV/m = "
       << P0/1e6 << " MW (loaded)" << endl;
  cout << "Peak fields (loaded): "
       << acs2_noIrisThickTaper.getMaxFields(P0, I_beam) << endl;

  cout << "Max beam time (loaded): "
       << acs2_noIrisThickTaper.getMaxAllowableBeamTime_detailed(P0, I_beam) << endl;
  cout << "Max beam time (loaded pulse, no beam): "
       << acs2_noIrisThickTaper.getMaxAllowableBeamTime_detailed(P0, I_beam, 0.0) << endl;
  cout << "Energy to beam = "
       << 1.60217646e-19*3.72e9*312 * acs2_noIrisThickTaper.getVoltageLoaded(P0, I_beam) << " J" << endl;
  cout << "Energy to structure = "
       << P0 * (acs2_noIrisThickTaper.getTfill()+ acs2_noIrisThickTaper.getTrise() + 312*0.5e-9) << " J" << endl;
  cout << "RF -> beam efficiency = "
       << acs2_noIrisThickTaper.getTotalEfficiency(P0, I_beam, 312*0.5e-9)*100 << " %" << endl;

  cout << "Min bunch spacing (6.6 V/pC/mm/m) "
       << acs2_noIrisThickTaper.getMinBunchSpacing(6.6) << " cycles" <<endl;

  cout << endl;
}
