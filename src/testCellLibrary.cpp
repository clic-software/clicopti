#include <iostream>
#include <cstddef>
#include <fstream>
#include <vector>

#include "cellParams.h"
#include "cellBase.h"

using namespace std;

int main(int argc, char* argv[]) {
  vector<size_t> offsets = {  
    offsetof(struct CellParams, psi),
    offsetof(struct CellParams, a_n),
    offsetof(struct CellParams, d_n) };
  CellBase_linearInterpolation base("cellBase/TD_30GHz.dat", 3, offsets);
  base.printGrid();

  cout << endl << endl << endl;
  cout << "TESTING CELL ARITHMETIC" << endl;
  vector<size_t> Idex(3,0);
  CellParams c000 = base.getCellGrid(Idex);
  cout << "C000 =\n\t" << c000 << endl;
  Idex[0] = 0; Idex[1] = 0; Idex[2] = 1;
  CellParams c001 = base.getCellGrid(Idex);
  cout << "C001 =\n\t" << c001 << endl;
  cout << "c000*2.0 =\n\t" << c000*2.0 << endl;
  cout << "c000+c001 =\n\t" << c000+c001 << endl;
  cout << "c000-c001 =\n\t" << c000-c001 << endl;
  cout << "(c000+c001)*0.5 =\n\t" << (c000+c001)*0.5 << endl;
  cout << "(c000+c001)/2.0 =\n\t" << (c000+c001)/2.0 << endl;
  // c000 *= 1.0;
  // cout << "c000*=1.0 =\n\t" << c000 << endl;
  // c000 *= 2.0;
  // cout << "c000*=2.0 =\n\t" << c000 << endl;
  // c001 += c001;
  // cout << "c001+=c001 =\n\t" << c001 << endl;

  //Test that the interpolation works as expected
  cout << endl << endl << endl;
  cout << "TESTING GRID INTERPOLATION" << endl;
  vector<double> dIdex(3);
  for (size_t Ipsi = 0; Ipsi < 2; Ipsi++) {
    Idex[0] = Ipsi;
    for (size_t Ian = 0; Ian < 5; Ian++) {
      Idex[1] = Ian;
      for (size_t Idn = 0; Idn < 3; Idn++) {
        Idex[2] = Idn;
        cout << "Idex = " << Idex[0]
             << " " << Idex[1] << " " << Idex[2] << endl;

        CellParams t = base.getCellGrid(Idex);
        cout << t << endl;

        dIdex[0] = getByOffset(t, offsets[0]);
        dIdex[1] = getByOffset(t, offsets[1]);
        dIdex[2] = getByOffset(t, offsets[2]);
        cout << "dIdex = " << dIdex[0]
             << " " << dIdex[1] << " " << dIdex[2] << endl;

        cout << base.getCellInterpolated(dIdex) << endl << endl;

        //return 0;
      }
    }
  }

  /** ** ** Testing CellBase_linearInterpolation_freqScaling ** ** **/
  Idex = {0,0,0};

  CellBase_linearInterpolation_freqScaling base2("cellBase/TD_30GHz.dat", 3, offsets, 11.9942);
  CellParams b000 = base2.getCellGrid(Idex);
  cout << "b000: " << endl << b000 << endl;
  base2.scaleCell(b000);
  cout << "Scaled: " << endl << b000 << endl;

  dIdex = {0,0,0};
  dIdex[0] = getByOffset(b000,offsets[0]); //Using unscaled parameters, so scalings doesn't matter.
  dIdex[1] = getByOffset(b000,offsets[1]);
  dIdex[2] = getByOffset(b000,offsets[2]);
  CellParams a000 = base2.getCellInterpolated(dIdex);
  cout << "Interpolated and internally scaled: " << endl << a000 << endl;

  /** ** ** Testing CellBase_compat ** ** **/
  cout << endl << endl << endl
       << "CellBase_compat: "  << endl;

  CellBase_compat base3("cellBase/TD_30GHz.dat",11.9942);
  base3.printGrid();
  dIdex = {0,0,0};
  cout << "Testing psi=120 deg:" << endl;
  double psi = 120;
  dIdex[0] = psi;
  for (double a_n = 0.07; a_n <= 0.23; a_n += 0.01) {
    for (double d_n = 0.1; d_n <=0.4; d_n +=0.1) {
      dIdex[1] = a_n;
      dIdex[2] = d_n;
      cout << a_n << " " << d_n << " " << base3.getCellInterpolated(dIdex) << endl;
    }
  }
  cout << endl << endl << endl;
  cout << "Testing psi=150 deg:" << endl;
  psi = 150;
  dIdex[0] = psi;
  for (double a_n = 0.07; a_n <= 0.23; a_n += 0.01) {
    for (double d_n = 0.1; d_n <=0.4; d_n +=0.1) {
      dIdex[1] = a_n;
      dIdex[2] = d_n;
      cout << a_n << " " << d_n << " " << base3.getCellInterpolated(dIdex) << endl;
    }
  }

  cout << endl << endl << endl;
  cout << "Testing psi=130 deg:" << endl;
  psi = 130;
  dIdex[0] = psi;
  for (double a_n = 0.07; a_n <= 0.23; a_n += 0.01) {
    for (double d_n = 0.1; d_n <=0.4; d_n +=0.1) {
      dIdex[1] = a_n;
      dIdex[2] = d_n;
      cout << a_n << " " << d_n << " " << base3.getCellInterpolated(dIdex) << endl;
    }
  }

  cout << endl << endl << endl;
  psi = 120;
  cout << "Writing test file at psi = " << psi << ", looping from (a_n, d_n) = (0.07,0.1) via (0.11,0.25) to (0.15,0.4):" << endl;
  dIdex[0] = psi;
  Idex = {0,0,0};
  CellParams tempCell = base3.getCellGrid(Idex);
  cout << "First  ref. cell = " << tempCell << endl;
  base3.scaleCell(tempCell);
  cout << "          scaled = " << tempCell << endl;

  Idex[0] = 0; Idex[1] = 2; Idex[2] = 1;
  tempCell = base3.getCellGrid(Idex);
  cout << "Middle ref. cell = " << tempCell << endl;
  base3.scaleCell(tempCell);
  cout << "          scaled = " << tempCell << endl;

  Idex[0] = 0; Idex[1] = 4; Idex[2] = 2;
  tempCell = base3.getCellGrid(Idex);
  cout << "Last   ref. cell = " << tempCell << endl;
  base3.scaleCell(tempCell);
  cout << "          scaled = " << tempCell << endl;

  ofstream ofile("/tmp/testCellLibrary_interpolCompatTest.dat");
  ofstream ofile2("/tmp/testCellLibrary_interpolLinear.dat");

  for (double t = 0; t <= 2.00000; t += 0.01) {
    //dIdex[1] = t*(0.11-0.07) + 0.07; //a_n
    dIdex[1] = t*(0.11-0.07)*2 + 0.07; //a_n
    dIdex[2] = t*(0.25-0.1 ) + 0.1;  //d_n
    CellParams tCell  = base3.getCellInterpolated(dIdex);
    CellParams tCell2 = base2.getCellInterpolated(dIdex);
    //cout << "t=" << t << " " << tCell << endl;
    ofile << t << " " << tCell.a_n << " " << tCell.d_n << " " << tCell.rQ << " " << tCell.Q << " " << tCell.vg << endl;
    ofile2 << t << " " << tCell2.a_n << " " << tCell2.d_n << " " << tCell2.rQ << " " << tCell2.Q << " " << tCell2.vg << endl;
  }

  ofile.close();
  ofile2.close();

  //Finally:
}
