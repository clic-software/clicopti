CITATION of CLICopti
================================================================================

We have invested a lot of time and effort in creating and maintaining
the CLICopti library, please cite it when using it.

To cite the CLICopti Library in publications use:

  * Implemented physics and C++ core (always cite this):

    K. Sjobak, A. Grudiev,
    "The CLICopti RF structure parameter estimator",
    CLIC-Note-1031, 2014,
    CERN, Geneva, Switzerland
    URL https://cds.cern.ch/record/1712948

  * If using the Octave or Python interface, also cite this:

    Jim Ögren, et al.,
    "Toolbox for optimization of RF efficiency for linacs",
    Proceedings of IPAC19, Melbourne, Australia May 19-24, 2019
    URL http://cds.cern.ch/record/2693528/files/weprb113.pdf

BibTeX entries for LaTeX users:

@techreport{Sjobak:1712948,
  author        = {Sjobak, Kyrre Ness and Grudiev, Alexej},
  title         = "{The CLICopti RF structure parameter estimator}",
  institution   = "CERN",
  address       = "Geneva",
  number        = "CERN-OPEN-2014-036, CLIC-Note-1031",
  month         = "Jun",
  year          = "2014",
  reportNumber  = "CERN-OPEN-2014-036",
  url           = "{https://cds.cern.ch/record/1712948}"
}

@inproceedings{Ogren_IPAC19,
  title     = "{Toolbox for optimization of RF efficiency for linacs}",
  author    = {Jim \"{O}gren et al.},
  year      = {2019},
  note      = {Proceedings of IPAC19, Melbourne, Australia May 19-24, 2019},
  url       = "{http://cds.cern.ch/record/2693528/files/weprb113.pdf}"
}

For further reading and references for some of the methods used in CLICopti,
please see the file REFERENCES.
