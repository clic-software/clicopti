% optimizing hdsmn structure for 150 MV/m
%a and d are (ap radius and iris thickness) / lambda 

close all
clear all

f = 12; %working frequency in GHz
Ea = 100; %[MV/m] averaged loaded accelerating gradient
dphi = 150; %[degree] phase advance per cell
eps = 'norm'

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% optimization procedure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dav = 0.01;%step for <a>/lambda
dda = 0.05;%step for (a1-a2)/<a>
dd = 0.02; %step for d/h

dav = 0.005;%step for <a>/lambda
dda = 0.01;%step for (a1-a2)/<a>

%%%%%%%%%%%%%%% calculate and save %%%%%%%%%%%%%
for Ea = [50,57,67,80,100]
    tic
    disp(sprintf('Ea = %d MV/m, f = %d GHz, dphi = %d deg',Ea,f,dphi))
    scanStructSpace500GeV_Nc26(eps, dphi, Ea, f, dav, dda, dd);
    scanStructSpace500GeV_Nc56(eps, dphi, Ea, f, dav, dda, dd);
    toc
end