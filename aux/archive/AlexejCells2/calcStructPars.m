function [tf,eff,es,dt,pio] = calcStructPars(Nc, dphi, f, Ea, Q, vg, rQ, Es, dT, Ne, Ncycl, Nb)
%tf [ns] - filling time
%eff [%] - rf-to-beam efficiency for rect. pulse
%es [MV/m] - max surface field in first/last cell
%dt [K] - max delta T in first/last cells
%pio [MW] - input/output power to maintain Ea in unloaded/loaded structure

%Nc - number of cells
%dphi [degree] - rf phase advance per cell
%f [GHz] - frequency
%Ea [MV/m] - averaged loaded accelerating gradient
%Q - Q-factor in first,middle,last cells
%vg [%] - group velocity to speed of light ratio in first,middle,last cells
%rQ [LinacOhm/m] - r upon Q in first,middle,last cells
%Es - max surface field to accelerating gradient in first,middle,last cells
%dT - max delta T for Eacc=150MV/m and tp=130ns in first,middle,last cells
%Ne [10^9] - bunch population
%Ncycl - bunch spacing in rf cycles
%Nb - number of bunches in the train

global EAC_LOAD EAC_UNLOAD %[MV/m]
global P_LOAD P_UNLOAD %[MW]

c = 299792458; %[m/s] - speed of light
e = 1.6e-19;   %[C] - electron charge
%e = 1.602176565e-19;   %[C] - electron charge

h = c/f*1e9*dphi/360;     %[m] - cell length
Ls = h*(Nc + 180/dphi);   %[m] - total (cells+couplers) structure length
Qb = e*Ne*Nb;             %[nC] - beam charge
tb = (Nb-1)*Ncycl/f;      %[ns] - beam length

ns = length(Nc);
tf = zeros(1,ns);
ea = zeros(2,ns);
pio = zeros(3,ns);
pun = zeros(1,ns);
for i = 1:ns
  [tf(i),pun(i)] = ...
    calcStructEacc(Nc(i), dphi, f, Ea, Q, vg, rQ, Ne, Ncycl);
  ea(:,i) = EAC_UNLOAD([1,Nc(i)])';%[MV/m]
  pio(1:2,i) = P_UNLOAD([1,Nc(i)+1])';%[MW]
  pio(3,i) = P_LOAD([Nc(i)+1]);%[MW]
end
es(1,:) = Es(1)*ea(1,:); %[MV/m]
es(2,:) = Es(3)*ea(2,:); %[MV/m]
tp = tf + tb;            %[ns] - rf pulse length
tpn = sqrt(tp/130);
dt(1,:) = tpn.*dT(1).*(ea(1,:)/150).^2; %[K] -for Ea and tp
dt(2,:) = tpn.*dT(3).*(ea(2,:)/150).^2; %[K] -for Ea and tp
%%%%%%%%%%%%
eff = 100*Ls*Ea*Qb./(pio(1,:).*tp); % [%] - rectangular pulse rf-to-beam efficiency
return



