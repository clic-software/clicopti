function scanStructSpace500GeV_Nc26(eps, dphi, Ea, f, dav, dda, dd)

c = 299.792458; %speed of light [mm/ns]
Ncycl0 = 6; %minimum number of RF periods between the bunches
Wxmax0 = 20; %reference max value of wake at the second bunch [V/pC/mm/m]
Nemax0 = 4; %reference max number of electrons per bunch [10^9]
Eamax0 = 150; %reference max avereged accelerrating gradient [MV/m]
Nb0 = 154; %reference number of bunches in the train

global EAC_LOAD EAC_UNLOAD %[MV/m]
global P_LOAD P_UNLOAD %[MW]

Ls_scan = [200,1000]; %[mm] - structure length range
h = c/f*dphi/360;    %[mm] - cell length
Nc_scan = fix(Ls_scan/h);
Nc_min = min(Nc_scan);
Nc_max = max(Nc_scan);
Nc_av = fix(sqrt(prod(Nc_scan)));
Nc_np1 = 10;
nc = round(Nc_min*(2*exp(log((Nc_max+Nc_min)/(2*Nc_min))/Nc_np1*[0:Nc_np1])-1));
Nc_np1_ = 0;
switch Nc_np1_
case 0 %single length
  Nc = 56;
  Nc = 45; %for 150 degree
  nc = Nc;
  nc_ = Nc;
  Nc_av = Nc;
  Nc_max = Nc;
case 3 %4 different types of structures
  nc_ = Nc_min:Nc_max;
otherwise
  nc_ = round(Nc_min*(2*exp(log((Nc_max+Nc_min)/(2*Nc_min))/Nc_np1_*[0:Nc_np1_])-1))
end
method = 'linear';
method1 = 'spline';
EAC_LOAD   = zeros(1,Nc_max);
EAC_UNLOAD = zeros(1,Nc_max);
P_LOAD     = zeros(1,Nc_max+1);
P_UNLOAD   = zeros(1,Nc_max+1);

initDatafromDaniel500GeV(eps,Ea);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% optimization procedure
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
av_min = 0.09; %a/lambda minimum
av_max = 0.21; %a/lambda maximum
da_min = dda;  %(a1-a2)/av minimum
da_max = 0.6;  %(a1-a2)/av maximum
d_min = 0.1;   %d/h minimum
d_max = 0.4;   %d/h maximum
d_min2 = d_min;
d_max2 = d_max;

av_m = av_min:dav:av_max;
navm = length(av_m);
da_m = da_min:dda:da_max;
ndam = length(da_m);
d_m = d_min:dd:d_max;
ndm = length(d_m);
d_m2 = d_min2:dd:d_max2;
ndm2 = length(d_m2);

Ne_m = zeros(navm,ndam);
Ncyc = zeros(navm,ndam,ndm,ndm2);
Ncel = zeros(navm,ndam,ndm,ndm2,Nc_np1_+1);
Esmx = zeros(navm,ndam,ndm,ndm2,Nc_np1_+1);
dTmx = zeros(navm,ndam,ndm,ndm2,Nc_np1_+1);
Effi = zeros(navm,ndam,ndm,ndm2,Nc_np1_+1);
Pinp = zeros(navm,ndam,ndm,ndm2,Nc_np1_+1);
Pout = zeros(navm,ndam,ndm,ndm2,Nc_np1_+1);
Poul = zeros(navm,ndam,ndm,ndm2,Nc_np1_+1);
tfil = zeros(navm,ndam,ndm,ndm2,Nc_np1_+1);

for i = 1:navm
  disp(sprintf('<a>/lambda = %5.3f',av_m(i)));
  Ne_m(i,:) = calcBunchCharge(Ea, av_m(i), da_m, f);
  if Ne_m(i,1) > 0
  for i2 = 1:ndam
    Ne = Ne_m(i,i2);
    a = av_m(i)*[1+da_m(i2)/2,1,1-da_m(i2)/2];
    for j = 1:ndm
      for j2 = 1:ndm2
        d = [d_m(j),(d_m(j)+d_m2(j2))/2,d_m2(j2)];
        if 1 %calculate min possible bunch spasing
          [Q1, f1, A1] = calcCellWake(a, d, f, dphi);
          Wx_max = Wxmax0*Nemax0/Ne*Ea/Eamax0;
          Ncycl = calcBunchSpacing(Nc_av, Q1, f1, A1, Wx_max, f);
          if Ncycl < Ncycl0
            Ncycl = Ncycl0;
          end
        else %take nominal bunch spacing
          Ncycl = Ncycl0;
        end
        Ncyc(i,i2,j,j2) = Ncycl;
        [Q, vg, rQ, Es, dT] = calcCellPars(a, d, f, dphi);   
        [tf, eff, es, dt, pio] = calcStructPars...
            (nc, dphi, f, Ea, Q, vg, rQ, Es, dT, Ne, Ncycl, Nb0);
        if Nc_np1_ == 0
          Ncel(i,i2,j,j2,:) = Nc;
          Esmx(i,i2,j,j2,:) = max(es);
          dTmx(i,i2,j,j2,:) = max(dt);
          Effi(i,i2,j,j2,:) = eff;
          Pinp(i,i2,j,j2,:) = pio(1);
          Pout(i,i2,j,j2,:) = pio(2);
          Poul(i,i2,j,j2,:) = pio(3);
          tfil(i,i2,j,j2,:) = tf;
        else
          es1 = interp1(nc,es(1,:),nc_,method);
          es2 = interp1(nc,es(2,:),nc_,method);
          dt1 = interp1(nc,dt(1,:),nc_,method);
          dt2 = interp1(nc,dt(2,:),nc_,method);
          Pin = interp1(nc,pio(1,:),nc_,method);
          Pou = interp1(nc,pio(2,:),nc_,method);
          Pou1= interp1(nc,pio(3,:),nc_,method);
          Eff = interp1(nc,eff,nc_,method1);
          tfi = interp1(nc,tf,nc_,method);
          if Nc_np1_ == 3
% finding Ncells for const Es structure
            [x,NcmEs] = min(abs(es1-es2));
            Ncel(i,i2,j,j2,1) = Nc_min-1+NcmEs;
            Esmx(i,i2,j,j2,1) = max(es1(NcmEs),es2(NcmEs));
            dTmx(i,i2,j,j2,1) = max(dt1(NcmEs),dt2(NcmEs));
            Effi(i,i2,j,j2,1) = Eff(NcmEs);
            Pinp(i,i2,j,j2,1) = Pin(NcmEs);
            Pout(i,i2,j,j2,1) = Pou(NcmEs);
            Poul(i,i2,j,j2,1) = Pou1(NcmEs);
            tfil(i,i2,j,j2,1) = tfi(NcmEs);
% finding Ncells for const dT structure
            [x,NcmdT] = min(abs(dt1-dt2));
            Ncel(i,i2,j,j2,2) = Nc_min-1+NcmdT;
            Esmx(i,i2,j,j2,2) = max(es1(NcmdT),es2(NcmdT));
            dTmx(i,i2,j,j2,2) = max(dt1(NcmdT),dt2(NcmdT));
            Effi(i,i2,j,j2,2) = Eff(NcmdT);
            Pinp(i,i2,j,j2,2) = Pin(NcmdT);
            Pout(i,i2,j,j2,2) = Pou(NcmdT);
            Poul(i,i2,j,j2,2) = Pou1(NcmdT);
            tfil(i,i2,j,j2,2) = tfi(NcmdT);
% finding Ncells for max Eff structure
            [x,NcmEf] = max(Eff);
            Ncel(i,i2,j,j2,3) = Nc_min-1+NcmEf;
            Esmx(i,i2,j,j2,3) = max(es1(NcmEf),es2(NcmEf));
            dTmx(i,i2,j,j2,3) = max(dt1(NcmEf),dt2(NcmEf));
            Effi(i,i2,j,j2,3) = Eff(NcmEf);
            Pinp(i,i2,j,j2,3) = Pin(NcmEf);
            Pout(i,i2,j,j2,3) = Pou(NcmEf);
            Poul(i,i2,j,j2,3) = Pou1(NcmEf);
            tfil(i,i2,j,j2,3) = tfi(NcmEf);
% finding Ncells for const P/a structure
            [x,NcmPa] = min(abs(Pin/a(1)-Pou/a(3)));
            Ncel(i,i2,j,j2,4) = Nc_min-1+NcmPa;
            Esmx(i,i2,j,j2,4) = max(es1(NcmPa),es2(NcmPa));
            dTmx(i,i2,j,j2,4) = max(dt1(NcmPa),dt2(NcmPa));
            Effi(i,i2,j,j2,4) = Eff(NcmPa);
            Pinp(i,i2,j,j2,4) = Pin(NcmPa);
            Pout(i,i2,j,j2,4) = Pou(NcmPa);
            Poul(i,i2,j,j2,4) = Pou1(NcmPa);
            tfil(i,i2,j,j2,4) = tfi(NcmPa);
          else
            Ncel(i,i2,j,j2,:) = nc_;
            Esmx(i,i2,j,j2,:) = max(es1,es2);
            dTmx(i,i2,j,j2,:) = max(dt1,dt2);
            Effi(i,i2,j,j2,:) = Eff;
            Pinp(i,i2,j,j2,:) = Pin;
            Pout(i,i2,j,j2,:) = Pou;
            Poul(i,i2,j,j2,:) = Pou1;
            tfil(i,i2,j,j2,:) = tfi;
          end
        end
      end
    end
  end
  end
end

save(sprintf('data500GeV_%s\\Nc45_E%df%ddphi%ddd%ddav%ddda%d',...
     eps,Ea,round(f),dphi,10000*dd,10000*dav,1000*dda),...
  'Ne_m', 'Ncyc', 'Ncel', 'Esmx', 'dTmx', 'Effi', 'Pinp', 'Pout', 'Poul', 'tfil');

