function Cem = calcElectricityCost(FOM)
% Electricity cost for 10 years
% parameters: FOM [a.u.]=[1e34/bx/m^2*%/1e9]
  Cem = (0.1011+7.1484./FOM)/12; % [a.u.=12GCHF]
  Cem = Cem*4;
return;