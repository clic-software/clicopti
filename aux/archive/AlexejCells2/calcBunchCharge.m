function N = calcBunchCharge(Ea, a, delta, f)
%Ea [MV/m] - averaged loaded accelerating gradient
%a - average iris radius over lambda
%delta - (a1-a2)/a
%f [GHz] - frequency

global FREQUENCY A_OVER_LAMBDA DA_OVER_LAMBDA BUNCH_CHARGE BUNCH_LENGTH LUMI_TOTAL LUMI_PEAK

nf = length(FREQUENCY);
na = length(A_OVER_LAMBDA);
nd = length(DA_OVER_LAMBDA);

if f == 12
  a0 = A_OVER_LAMBDA(1);
  da = A_OVER_LAMBDA(2)-a0;
  ja = (a-a0)/da+1;
  ja0 = floor(ja);
  if ja0 > 1 & abs(ja0-ja) < 0.000001
    ja0 = ja0 - 1;
  end
  wa = ja-ja0;
  n = BUNCH_CHARGE(:,ja0,1)*(1-wa) ...
    + BUNCH_CHARGE(:,ja0+1,1)*wa;
  N = interpQuad(n', DA_OVER_LAMBDA, delta);
else
  N = 0;
end
return;
