function y = initDatafromDaniel(d2h)

global FREQUENCY A_OVER_LAMBDA DA_OVER_LAMBDA BUNCH_CHARGE BUNCH_LENGTH LUMI_TOTAL LUMI_PEAK

FREQUENCY = 12; %[GHz]
A_OVER_LAMBDA = 0.07:0.01:0.15; % aol
DA_OVER_LAMBDA = 0:0.3:0.6; % aol_1-aol_2
nf = length(FREQUENCY);
na = length(A_OVER_LAMBDA);
nd = length(DA_OVER_LAMBDA);
BUNCH_CHARGE = zeros(nd,na,nf); %[10^9] - bunch population
BUNCH_LENGTH = zeros(nd,na,nf); %[mkm] - bunch population
LUMI_TOTAL = zeros(nd,na,nf);   %[1e34/m^2/bx] - total luminosity
LUMI_PEAK = zeros(nd,na,nf);   %[1e34/m^2/bx] - peak luminosity

if d2h == 0.4 %iris thickness d to cell length h ratio
    file = fopen('DatafromDaniel_12GHz100MVm_20070827_result_g4_new.12','r');
elseif d2h == 0.25 %iris thickness d to cell length h ratio
    file = fopen('DatafromDaniel_12GHz100MVm_20070827_result_g2_new.12','r');
else %iris thickness d to cell length h ratio
    file = fopen('DatafromDaniel_12GHz100MVm_20070827_result_g1_new.12','r');
end
data = fscanf(file, '%g', [7, nd*na*nf]);
fclose(file);
for jf = 1:nf
  for jd = 1:nd
    index = (jf-1)*na*nd+(nd-jd)*na+[na:-1:1]; 
    BUNCH_CHARGE(jd,:,jf) = data(3,index); 
    BUNCH_LENGTH(jd,:,jf) = data(4,index); 
    LUMI_TOTAL(jd,:,jf) = 1e-34*data(6,index); 
    LUMI_PEAK(jd,:,jf) = 1e-34*data(7,index);
  end
end
return;

a = A_OVER_LAMBDA;
N = BUNCH_CHARGE;
L = LUMI_PEAK;
for jd = 1
  plot(a,L(jd,:,jf)./N(jd,:,jf),'b-o'); hold on
end
return;
