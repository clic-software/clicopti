function [x] = interpCellPars(a, d, d_n, xmn)
% parameters at 29.985 GHz
a_m = [0.07,0.11,0.15,0.19,0.23]; %a/lambda
x_m = interpQuad(xmn, d_n, d);
ia = round((a-a_m(1))/(a_m(2)-a_m(1)))+1;
x = zeros(1,length(a));
for i = 1:length(a)
  ind = ia(i);
  if ind < 2
    ind = 2;
  elseif ind > 4
    ind = 4;
  end
  ind = ind + [-1,0,1];
  x(i) = interpQuad(x_m(ind,i)', a_m(ind), a(i));
end
return;

