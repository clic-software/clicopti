import numpy as np
import scipy as sp
import scipy.interpolate
from GeomParams import GeomParams

class GeomParams_502(GeomParams):

    name = "CLIC_502"
    
    #### GEOMETRY PARAMETERS ###

    #Number of regular cells
    N = 22

    #Period 150 deg @ 11.994 GHz, vph=c
    period = 10.41467e-3 #[m] (NOTE UNITS - THIS IS NOT mm!!!)
    
    #Radius of power input/output WG and beampipe in mm
    #dcwg = 21.9
    #Cavity roundoff radius in mm
    #rr = 0.5
    #Damping waveguide entrance in mm
    #idw = 8
    #Damping waveguide dimension in mm
    #adw = 11
    #Damping waveguide length in mm
    #ldw = 40
    
    #Radius of irises in mm,
    # including matching irises at end of structure.
    # Usually called "a".
    irisRadii = None
    #Thickness of irises in mm,
    # including matching irises at ends of structure.
    # Usually called "d".
    irisThick = None
    #Ellipticity of iris,
    # Usually called "e"
    irisE = None
    #Cell outer radius in mm,
    # including matching cells at ends of structure.
    # Usually called "b".
    cellRadii = None
    #Cell outer wall flat part length in mm,
    # including matching cells at ends of structure.
    # Usually called "c".
    cellFlat = None
    #Cell outer wall elipticity,
    # including matching cells at end of structure.
    # Usually called "eow"
    cellEow = None
    #Cell cavity length in mm.
    # Usually called "l".
    # Total element length L = l+d = disk length (!= period which is between iris tips!!)
    cellLength = None

    ### RF PARAMETERS (first/mid/last) ###
    f     = np.asarray([11.9942, 11.9942, 11.9942]) #[GHz]
    Q     = np.asarray([6364.8, 6370.5, 6383])
    vg    = np.asarray([2.056, 1.614, 1.234]) #[% of c]  #THE REAL THING
    #vg    = np.asarray([2.056, 1.3, 0.4]) #[% of c] 
    RoQ   = np.asarray([10304.92, 11213.4, 12175.9]) #[Ohm/m]

    # Structure input
    #P0 = 61.3e6 #Peak input power [W]
    P0 = 74.1942303887e6 #22 cells
    #P0 = 72.0934779239e6 #21 cells
    #P0 = 70.030304919e6 #20 cells
    #P0 = 68.0043950585e6 #19 cells
    I  = 1.60217646e-19*6.8e9/0.5e-9 #Beam current [A]
    Ez_target = 80e6 #[MV/m]

    # Peak fields (first/mid/last)
    Emax = np.asarray([2.25, 2.23, 2.22]) #[-]
    Hmax = np.asarray([4.684, 4.511, 4.342])  #[mA/V]
    SCmax = np.asarray([0.493, 0.435, 0.381])#[mA/V]
    
    trise = 15.3e-9 #[s]
    #tfill #Calculated from vg
    tbeam = 354*0.5e-9 #[s]
    
    # Material properties (OFC copper) for thermal calcs
    # SLAC report 577
    rho = 8.95e3 #[kg/m^3] Mass density
    ceps = 385.0    #[J/(kg*K)] Specific heat capacity
    ktherm = 391.0  #[W/(m*K)] thermal conductivity
    sigmaElectric = 5.8e7 #[1/(ohm*meter)] electric conductivity
    mu0 = 4e-7*np.pi #[H/m] magnetic permeability

    #Wakefields for first/last cell's modes
    # [[mode1/cell1, mode1/cell2,..], [mode2/cell1, mode2/cell2,..],..]
    w_k = [[52.78076151,54.50428264], [79.06226975,77.7744841]]
    w_Q = [[16.78249817,9.16779521],  [18.5437367,18.45525816]]
    w_A = [[77.23336969,112.422794],  [21.69354865,44.80380496]]

    def __init__(self):

        #Ellipticity of irises
        #self.irisE = 1 + (self.irisThick/self.period) / (self.irisRadii / 2.625)

        #Check that we have the right ammount of parameters
        # assert len(self.irisRadii) == \
        #        len(self.irisThick) == \
        #        len(self.irisE)     == \
        #        self.N+3
        # assert len(self.cellRadii)  == \
        #        len(self.cellFlat)   == \
        #        len(self.cellEow)    == \
        #        len(self.cellLength) == \
        #        self.N+2


        # setup RF parameter interpolation functions
        self.omega         = np.mean(self.f)*1e9*(2*np.pi)
        print "OMEGA =", self.omega
        self.Zcell = map(self.getZ_Cell, [1, (self.N+1.0)/2.0, -1])
        print "Zcell =", self.Zcell
        self.Ziris = map(self.getZ_Iris, [1, (self.N+2.0)/2.0, -1])
        print "Ziris =", self.Ziris
        self.Q_interp      = sp.interpolate.lagrange(self.Ziris, self.Q) # [-]
        self.vg_interp     = sp.interpolate.lagrange(self.Ziris, self.vg*3e8/100.0) # [m/s]
        self.RoQ_interp    = sp.interpolate.lagrange(self.Ziris, self.RoQ) # [Ohm/m]
        
        self.Emax_interp   = sp.interpolate.lagrange(self.Ziris, self.Emax)  # [-]
        self.Hmax_interp   = sp.interpolate.lagrange(self.Ziris, self.Hmax)  # [mA/V]
        self.SCmax_interp  = sp.interpolate.lagrange(self.Ziris, self.SCmax) # [mA/V]

        print
        self.skinDepth = np.sqrt(2/(self.mu0*self.sigmaElectric*self.omega))
        print "SkinDepth =", self.skinDepth*1e6, "[um]"
        self.surfResistance = np.sqrt(self.mu0*self.omega/(2*self.sigmaElectric))
        print "SurfResistance =", self.surfResistance, "[ohms]"
        print

        
        self.w_k_interp = [sp.interpolate.lagrange(map(self.getZ_Cell, [1,-1]),self.w_k[0]),\
                           sp.interpolate.lagrange(map(self.getZ_Cell, [1,-1]),self.w_k[1])]
        self.w_Q_interp = [sp.interpolate.lagrange(map(self.getZ_Cell, [1,-1]),self.w_Q[0]),\
                           sp.interpolate.lagrange(map(self.getZ_Cell, [1,-1]),self.w_Q[1])]
        self.w_A_interp = [sp.interpolate.lagrange(map(self.getZ_Cell, [1,-1]),self.w_A[0]),\
                           sp.interpolate.lagrange(map(self.getZ_Cell, [1,-1]),self.w_A[1])]
        # For testing: All cells = first cell
        #self.w_k_interp = [lambda z: self.w_k[0][0],lambda z: self.w_k[1][0]]
        #self.w_Q_interp = [lambda z: self.w_Q[0][0],lambda z: self.w_Q[1][0]]
        #self.w_A_interp = [lambda z: self.w_A[0][0],lambda z: self.w_A[1][0]]
