#!/usr/bin/env python

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import ticker

from matplotlib import rcParams
rcParams.update({'text.usetex': True}) #slow
#rcParams.update({'font.size': 22})



import sys

#Pessimistic
CEs = 220 #^6 (MV/m)^6 * 200ns
CSc = 4.0 #^3 (MW/mm^2)^3 * 200 ns
CPC = 2.3 #^3 (MW/mm)^3 * 200 ns
#Optimistic
# CEs = 250 #^6 (MV/m)^6 * 200ns
# CSc = 5.0 #^3 (MW/mm^2)^3 * 200 ns
# CPC = 2.9 #^3 (MW/mm)^3 * 200 ns



def LoadData(fname):
    (an, dn, a, Q, vg, rQ, Es, Hs, Sc, fW, QW, AW) = np.loadtxt(fname, unpack=True)
    #Detect the shape - first (slowest)idx an, 2nd (fastest) idx is dn
    shape1 = None # Number of unique an
    shape2 = None # Number of unique dn
    an_sequence = []; an_sequence.append(an[0])
    dn_sequence = []; dn_sequence.append(dn[0])
    for i in xrange (1,len(an)):
        if an[i] != an_sequence[-1]: #new row detected
            an_sequence.append(an[i])
            if shape2 == None:
                shape2 = i
            else:
                if i % shape2 != 0:
                    print "Error: shape2 not consistent?!?"
                    print "an = " + str(an)
                    print "Detected shape2 =", shape2
                    exit(1)
            
        if shape2 == None: #On first row
            dn_sequence.append(dn[i])
        else:
            if dn_sequence[i%shape2] != dn[i]:
                print "Error: dn not reapeating itself"
                print "dn =", dn
                print "dn_sequence =", dn_sequence
                exit(1)
    shape1 = len(an_sequence)

    print "detected an_sequence =", an_sequence
    print "detected dn_sequence =", dn_sequence

    shape = (shape1,shape2)
    an = an.reshape(shape)
    dn = dn.reshape(shape)
    a  =  a.reshape(shape) # m
    Q  =  Q.reshape(shape) 
    vg = vg.reshape(shape) # %c
    rQ = rQ.reshape(shape) # Ohm/m
    Es = Es.reshape(shape)
    Hs = Hs.reshape(shape) # mA/V
    Sc = Sc.reshape(shape) # mA/V
    fW = fW.reshape(shape) # GHz
    QW = QW.reshape(shape)
    AW = AW.reshape(shape) # V/pC/mm/m
    
    # (MV/m)^6 * ns
    t_Es  = ( CEs**6 * 200.0) / Es**6
    t_Sc  = ( CSc**3 * 200.0) / (Sc*1e-3)**3
    #t_Sc = 1.0/np.zeros_like(t_Es) #Emulate 30 GHz DB
    t_PC  = 1e-9*( CPC**3 * 200.0) * ( 2*np.pi*a * 2*np.pi*11.9942e9 * rQ / (vg*3e8/100) )**3 # (MV/m)^6 ns
    #Find minimum
    t = np.empty_like(t_Es)
    for i in xrange(t.shape[0]):
        for j in xrange(t.shape[1]):
            t[i,j]=min(t_Es[i,j],t_Sc[i,j], t_PC[i,j])

    return (an,dn,a, Q,vg,rQ, Es,Hs,Sc, t_Es,t_Sc,t_PC,t, fW,QW,AW)

if len(sys.argv) == 1:
    print "Usage: testCellLibrary_compareDBs-PLOT.py datafile1--name1 datafile2--name2 ..."
    exit(1)

#Parse input arguments
def USAGE():
    print "Usage: ./testCellLibrary_compareDBs-PLOT.py databasedump datafile [3D]"
    
if len(sys.argv) < 3 or len(sys.argv) > 5:
    USAGE();
    print "wrong number of arguments"
    exit(1)
databasedump = sys.argv[1]
datafile = sys.argv[2]

use3D = False
comparisonFileName = None
if len(sys.argv) > 3 :
    if sys.argv[3] == "3D":
        use3D = True
        from mpl_toolkits.mplot3d import axes3d
        #from matplotlib import cm
    else:
        USAGE()
        print "3rd argument (optional) must be '3D'"
        exit(1)
    if len(sys.argv) == 5:
        comparisonFileName = sys.argv[4]

plotfileName = datafile[:-4] #Strip off .dat
if use3D:
    plotfileName += "-3D"
plotfileName += "-plots"

numContours = 20

print "Got settings:"
print " - databasedump = '" + databasedump + "'"
print " - datafile     = '" + datafile + "'"
print " - use3D        =  " + str(use3D)
print " - comparisonFileName = '" + str(comparisonFileName) + "'"
print " - numContours  =  " + str(numContours)

import os
if not os.path.isdir(plotfileName):
    print "Making folder '" + plotfileName + "'"
    os.mkdir(plotfileName)
plotfileName = os.path.join(plotfileName,plotfileName)

print "Reading databasedump file '" + databasedump + "'"
(an_dump,dn_dump,a_dump, Q_dump,vg_dump,rQ_dump, Es_dump,Hs_dump,Sc_dump, t_Es_dump,t_Sc_dump,t_PC_dump,t_dump, fW_dump,QW_dump,AW_dump) = LoadData(databasedump)

#Where the simulations where ran
points_an = an_dump.flatten()
points_dn = dn_dump.flatten()

if comparisonFileName:
    print "Reading comparison file '" + comparisonFileName + "'"
    (an_comp,dn_comp,a_comp, Q_comp,vg_comp,rQ_comp, Es_comp,Hs_comp,Sc_comp, t_Es_comp,t_Sc_comp,t_PC_comp,t_comp, fW_comp,QW_comp,AW_comp) = LoadData(comparisonFileName)

print "Reading main data file '"+datafile+"'"
(an,dn,a, Q,vg,rQ, Es,Hs,Sc, t_Es,t_Sc,t_PC,t, fW,QW,AW) = LoadData(datafile)

print "Plotting..."

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,Q, numContours);
    plt.colorbar().set_label("$Q$")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,Q)
    #ax.plot_surface(an,dn,Q,cmap=cm.coolwarm,shade=True,linewidth=1,antialiased=True)
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),Q_dump.flatten(),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,Q_comp, color='gray')
    ax.set_xlabel("$a/\lambda$")
    ax.set_ylabel("$d/h$")
    ax.set_zlabel("$Q$")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-Q.png")
plt.savefig(plotfileName + "-Q.pdf")
    
fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,vg, numContours)#, vmin=0.0,vmax=12.6);
    plt.colorbar().set_label(r"$v_g/c$ [\%]")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,vg)
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),vg_dump.flatten(),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,vg_comp, color='gray')
    ax.set_xlabel("$a/\lambda$")
    ax.set_ylabel("$d/h$")
    ax.set_zlabel(r"$v_g/c [\%]$")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-vg.png")
plt.savefig(plotfileName + "-vg.pdf")

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,rQ, numContours);
    plt.colorbar().set_label(r"$R/Q/\mathbf{L}$ [$\Omega$/m]")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,rQ)
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),rQ_dump.flatten(),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,rQ_comp, color='gray')
    ax.set_xlabel("$a/\lambda$")
    ax.set_ylabel("$d/h$")
    ax.set_zlabel(r"$R/Q/\mathbf{L}$ [$\Omega$/m]")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-rQ.png")
plt.savefig(plotfileName + "-rQ.pdf")

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,Es, numContours);
    plt.colorbar().set_label(r"$\bar E_s$")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,Es)
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),Es_dump.flatten(),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,Es_comp, color='gray')
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"$\bar E_s$")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-Es.png")
plt.savefig(plotfileName + "-Es.pdf")
    
fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,Hs, numContours)#, vmin=3.3,vmax=5.7);
    plt.colorbar().set_label(r"$\bar H_s$ [mA/V]");
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,Hs)
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),Hs_dump.flatten(),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,Hs_comp, color='gray')
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"$\bar H_s$ [mA/V]")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-Hs.png")
plt.savefig(plotfileName + "-Hs.pdf")

if np.isnan(Sc).any() or np.isnan(Sc_dump).any():
    print "Sc or Sc_dump array contained at least one NaN - skipping!"
else:
    fig = plt.figure()
    if not use3D:
        plt.contourf(an,dn,Sc, numContours);
        plt.colorbar().set_label(r"$\bar S_c$ [mA/V]")
        plt.plot(points_an, points_dn, 'k*');
        plt.xlim(min(points_an),max(points_an))
        plt.ylim(max(points_dn),min(points_dn))
        plt.xlabel("$a/\lambda$")
        plt.ylabel("$d/h$")
    else:
        ax = fig.add_subplot(111,projection='3d')
        ax.plot_wireframe(an,dn,Sc)
        ax.scatter(an_dump.flatten(),dn_dump.flatten(),Sc_dump.flatten(),c='red',s=40)
        if comparisonFileName:
            ax.plot_wireframe(an_comp,dn_comp,Sc_comp, color='gray')
        ax.set_xlabel(r"$a/\lambda$")
        ax.set_ylabel(r"$d/h$")
        ax.set_zlabel(r"$\bar S_c$ [mA/V]")
        plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
    plt.savefig(plotfileName + "-Sc.png")
    plt.savefig(plotfileName + "-Sc.pdf")

# plt.figure()
# plt.contourf(an,dn,t**(1.0/6.0), numContours)#, locator=ticker.LogLocator(subs=[1,5]));
# plt.colorbar().set_label(r"Breakdown strenght index $\kappa^{1/6} = \tau^{1/6} \cdot G ~\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right) \mathrm{ns}^{1/6}\right]$")
# plt.plot(points_an, points_dn, 'k*');
# plt.xlim(min(points_an),max(points_an))
# plt.ylim(max(points_dn),min(points_dn))
# plt.xlabel("$a/\lambda$")
# plt.ylabel("$d/h$")
# plt.savefig(plotfileName + "-kappa16.png")

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,(t/200.0)**(1.0/6.0), numContours)#,vmin=35,vmax=140)#, locator=ticker.LogLocator(subs=[1,5]));
    plt.colorbar().set_label(r"Max gradient $G$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,(t/200.0)**(1.0/6.0))
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),(t_dump.flatten()/200.0)**(1.0/6.0),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,(t_comp/200.0)**(1.0/6.0), color='gray')
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"Max gradient $G$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-kappa16-200.png")
plt.savefig(plotfileName + "-kappa16-200.pdf")

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,(t_Es/200.0)**(1.0/6.0), numContours)#,vmin=35,vmax=140)#, locator=ticker.LogLocator(subs=[1,5]));
    plt.colorbar().set_label(r"Max gradient $G_{E}$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,(t_Es/200.0)**(1.0/6.0))
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),(t_Es_dump.flatten()/200.0)**(1.0/6.0),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,(t_Es_comp/200.0)**(1.0/6.0), color='gray')
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"Max gradient $G_E$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-kappa16-200_Es.png")
plt.savefig(plotfileName + "-kappa16-200_Es.pdf")

if np.isnan(t_Sc).any() or np.isnan(t_Sc_dump).any():
    print "t_Sc or t_Sc_dump array contained at least one NaN - skipping!"
else:
    fig = plt.figure()
    if not use3D:
        plt.contourf(an,dn,(t_Sc/200.0)**(1.0/6.0), numContours)#,vmin=35,vmax=140)#, locator=ticker.LogLocator(subs=[1,5]));
        plt.colorbar().set_label(r"Max gradient $G_{S_c}$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
        plt.plot(points_an, points_dn, 'k*');
        plt.xlim(min(points_an),max(points_an))
        plt.ylim(max(points_dn),min(points_dn))
        plt.xlabel("$a/\lambda$")
        plt.ylabel("$d/h$")
    else:
        ax = fig.add_subplot(111,projection='3d')
        ax.plot_wireframe(an,dn,(t_Sc/200.0)**(1.0/6.0))
        ax.scatter(an_dump.flatten(),dn_dump.flatten(),(t_Sc_dump.flatten()/200.0)**(1.0/6.0),c='red',s=40)
        if comparisonFileName:
            ax.plot_wireframe(an_comp,dn_comp,(t_Sc_comp/200.0)**(1.0/6.0), color='gray')
        ax.set_xlabel(r"$a/\lambda$")
        ax.set_ylabel(r"$d/h$")
        ax.set_zlabel(r"Max gradient $G_{S_c}$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
        plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
    plt.savefig(plotfileName + "-kappa16-200_Sc.png")
    plt.savefig(plotfileName + "-kappa16-200_Sc.pdf")

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,(t_PC/200.0)**(1.0/6.0), numContours)#,vmin=35,vmax=140)#, locator=ticker.LogLocator(subs=[1,5]));
    plt.colorbar().set_label(r"Max gradient $G_{P/C}$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,(t_PC/200.0)**(1.0/6.0))
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),(t_PC_dump.flatten()/200.0)**(1.0/6.0),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,(t_PC_comp/200.0)**(1.0/6.0), color='gray')
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"Max gradient $G_{P/C}$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-kappa16-200_PC.png")
plt.savefig(plotfileName + "-kappa16-200_PC.pdf")

if use3D:
    fig = plt.figure()
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,(t_Es/200.0)**(1.0/6.0), color='blue',rstride=4,cstride=4, linewidth=2)
    ax.plot_wireframe(an,dn,(t_Sc/200.0)**(1.0/6.0), color='green',rstride=4,cstride=4, linewidth=2)
    ax.plot_wireframe(an,dn,(t_PC/200.0)**(1.0/6.0), color='red',rstride=5,cstride=4, linewidth=2)
    #ax.scatter(an_dump.flatten(),dn_dump.flatten(),(t_PC_dump.flatten()/200.0)**(1.0/6.0),c='red',s=40)
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"Max gradient $G$ $\left[\left(\frac{\mathrm{MV}}{\mathrm{m}}\right)\right]$ @ 200 ns")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,fW, numContours)#, vmin=14.2,vmax=17.8);
    plt.colorbar().set_label(r"Wake freq. [GHz]")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,fW)
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),fW_dump.flatten(),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,fW_comp, color='gray')
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"Wake freq. [GHz]")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-fW.png")
plt.savefig(plotfileName + "-fW.pdf")

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,QW, numContours)#,vmin=6,vmax=28);
    plt.colorbar().set_label(r"Wake Q-factor")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,QW)
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),QW_dump.flatten(),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,QW_comp, color='gray')
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"Wake Q-factor")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-QW.png")
plt.savefig(plotfileName + "-QW.pdf")

fig = plt.figure()
if not use3D:
    plt.contourf(an,dn,AW, numContours)#, vmin=30,vmax=270);
    plt.colorbar().set_label(r"Wake Amplitude [V/pC/mm/m]")
    plt.plot(points_an, points_dn, 'k*');
    plt.xlim(min(points_an),max(points_an))
    plt.ylim(max(points_dn),min(points_dn))
    plt.xlabel("$a/\lambda$")
    plt.ylabel("$d/h$")
else:
    ax = fig.add_subplot(111,projection='3d')
    ax.plot_wireframe(an,dn,AW)
    ax.scatter(an_dump.flatten(),dn_dump.flatten(),AW_dump.flatten(),c='red',s=40)
    if comparisonFileName:
        ax.plot_wireframe(an_comp,dn_comp,AW_comp, color='gray')
    ax.set_xlabel(r"$a/\lambda$")
    ax.set_ylabel(r"$d/h$")
    ax.set_zlabel(r"Wake Amplitude [V/pC/mm/m]")
    plt.subplots_adjust(left=0.0, bottom=0.0, right=1.0, top=1.0)
plt.savefig(plotfileName + "-AW.png")
plt.savefig(plotfileName + "-AW.pdf")


plt.show()
